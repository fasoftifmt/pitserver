package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.*;
import br.edu.ifmt.pit.dominio.Usuario;
import br.edu.ifmt.pit.dominio.enums.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import br.edu.ifmt.pit.repositorio.interfaces.*;

import java.util.*;

@SuppressWarnings("Duplicates")
@Service
public class DBService {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;
    @Autowired
    private DepartamentoRepositorio departamentoRepositorio;
    @Autowired
    private CursoRepositorio cursoRepositorio;
    @Autowired
    private AtividadePitRepositorio atividadePitRepositorio;
    @Autowired
    private DisciplinaRepositorio disciplinaRepositorio;
    @Autowired
    private CategoriaAtividadeRepositorio categoriaAtividadeRepositorio;
    @Autowired
    private ServidorRepositorio servidorRepositorio;
    @Autowired
    private PlanoDeTrabalhoRepositorio planoDeTrabalhoRepositorio;
    @Autowired
    private EnvioRepositorio envioRepositorio;
    @Autowired
    private AtividadeRepositorio atividadeRepositorio;
    @Autowired
    private GrupoAtividadeRepositorio grupoAtividadeRepositorio;
    @Autowired
    private CampusRepositorio campusRepositorio; // ALTERADO
    @Autowired
    private AvaliadorRepositorio avaliadorRepositorio;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String tipo;

    public void instantiateDatabase() {

        if (!tipo.equalsIgnoreCase("create")) return;

        var camp01 = new Campus(null, "Cuiabá");    // ALTERADO
        var camp02 = new Campus(null, "Bela Vista"); // ALTERADO
        var camp03 = new Campus(null, "Rondonópolis");  // ALTERADO
        var camp04 = new Campus(null, "Barra do Garça");    // ALTERADO

        var dep01 = new DepartamentoArea(null, "Informatica", "DCOM", camp01);
        var dep02 = new DepartamentoArea(null, "Construção Civil", "DACC", camp01);
        var dep03 = new DepartamentoArea(null, "Eletro e eletrônica", "ELT", camp01);

        var curso01 = new Curso(null, "Sistemas para internet", dep01);
        var curso02 = new Curso(null, "Informática", dep01);
        var curso03 = new Curso(null, "Engenharia da computação", dep01);
        var curso04 = new Curso(null, "Controle de obras", dep02);
        var curso05 = new Curso(null, "Engenharia civil", dep02);
        var curso06 = new Curso(null, "Eletronica", dep03);

        var disc01 = new Disciplina(null, "Linguagem de programação I", 90.0, 6, 1,curso01);
        var disc02 = new Disciplina(null, "Linguagem de programação II", 90.0,6,2, curso01);
        var disc03 = new Disciplina(null, "Fundamentos de banco de dados", 60.0, 4, 1 ,curso01);

        var grupAtvDeEnsino = new GrupoAtividade(null, "Atividades De Ensino", 1 );
        var grupAtvPesquisa = new GrupoAtividade(null, "Atividades De Pesquisa Preferencialmente Aplicada e inovação", 2);
        var grupAtvExtensao = new GrupoAtividade(null, "Atividades De Extensão", 3);
        var grupAtvGestao = new GrupoAtividade(null, "Atividades De Gestão e representação institucional", 4);

        var catAtvOrientacaoGrupoGestao = new CategoriaAtividade(null, "Atividades de Orientação", grupAtvGestao, 1);
        var catAtvOrientacaoGrupoExtensao = new CategoriaAtividade(null, "Atividades de Orientação", grupAtvExtensao, 1);
        var catAtvOrientacaoGrupoPesquisa = new CategoriaAtividade(null, "Atividades de Orientação", grupAtvPesquisa, 1);
        var catAtvOrientacaoGrupoEnsino = new CategoriaAtividade(null, "Atividades de Orientação", grupAtvDeEnsino, 4);
        var catAtvAulasGrupoEnsino = new CategoriaAtividade(null, "Regência - Aulas", grupAtvDeEnsino, 1);
        var catAtvPreparacaoGrupoEnsino = new CategoriaAtividade(null, "Atividades de preparação e manutenção do ensino", grupAtvDeEnsino, 2);
        var catAtvApoioEnsinoGrupoEnsino = new CategoriaAtividade(null, "Atividades de Apoio ao Ensino", grupAtvDeEnsino, 3);


        // Instancia os usuários

        var usuario01 = new Usuario(null, "21232f297a57a5a743894a0e4a801fc3", bCryptPasswordEncoder.encode("21232f297a57a5a743894a0e4a801fc3"));
        usuario01.setPerfilUsuarioSet(new HashSet<>(Arrays.asList(PerfilUsuario.ADMIN)));
        var usuario02 = new Usuario(null, "0e1177622dc1d5506b5add8829b504a7", bCryptPasswordEncoder.encode("f8032d5cae3de20fcec887f395ec9a6a"));
        usuario02.setPerfilUsuarioSet(new HashSet<>(Arrays.asList(PerfilUsuario.SERVIDOR)));
        var usuario03 = new Usuario(null, "a2baaf0f83b59aac824aa705d86cd550", bCryptPasswordEncoder.encode("f8032d5cae3de20fcec887f395ec9a6a"));
        usuario03.setPerfilUsuarioSet(new HashSet<>(Arrays.asList(PerfilUsuario.GESTOR)));

        // Instancia os Servidores
        var doct01 = new Servidor("Fulano", "fulano@cba.com.br", "58197762082", "65 981225343", dep01,
                RegimeTrabalho.QUARENTA_HORAS, TipoDeVinculo.EFETIVO, SituacaoServidor.ATIVO);
        doct01.setUsuario(usuario01);

        var doct02 = new Servidor("João", "joao@cba.com.br", "57723210051", "65 981225345", dep02,
                RegimeTrabalho.VINTE_HORAS, TipoDeVinculo.EFETIVO, SituacaoServidor.INATIVO);
        doct02.setUsuario(usuario02);

        var doct03 = new Servidor("Maria", "maria@cba.com.br", "54932593031", "65 981225344", dep03,
                RegimeTrabalho.QUARENTA_HORAS, TipoDeVinculo.EFETIVO, SituacaoServidor.AFASTADO);
        doct03.setUsuario(usuario03);

        var doct04 = new Servidor("José", "jose@cba.com.br", "75891761092", "65 981225341", dep01, RegimeTrabalho.VINTE_HORAS,
                TipoDeVinculo.TEMPORARIO, SituacaoServidor.ATIVO);

        // Instancia o plano de trabalho
        var plan01 = new PlanoIndividualDeTrabalho(null, new Date(), StatusPit.PLANEJADO, doct02);

        var atividadePitList = new ArrayList<AtividadePit>();

        // Instancias as atividades
        List<Atividade> atividadesEnsino = this.instanciarAtividadesEnsino(catAtvAulasGrupoEnsino, atividadePitList, plan01);
        catAtvAulasGrupoEnsino.setAtividadeModelList(atividadesEnsino);


        List<Atividade> atividadesOrientacao = this.instanciarAtividadesOrientacao(catAtvOrientacaoGrupoEnsino, atividadePitList, plan01);
        catAtvOrientacaoGrupoEnsino.setAtividadeModelList(atividadesOrientacao);

        // sub-atividades
        List<Atividade> subAtividadeList = new ArrayList<>();
        var sub01 = new Atividade(catAtvPreparacaoGrupoEnsino, "Preparação + Planejamento",40.0,"dica" ,0.8, UnidadeAtividade.HORAS, false );
        var sub02 = new Atividade(catAtvApoioEnsinoGrupoEnsino,"Atendimento a Estudantes",40.0, "dica",0.2 ,UnidadeAtividade.HORAS, false);
        sub01.setSubAtividade(true);
        sub02.setSubAtividade(true);

        catAtvPreparacaoGrupoEnsino.addAtividade(sub01);
        catAtvApoioEnsinoGrupoEnsino.addAtividade(sub02);

        atividadesEnsino.forEach(atividade -> atividade.setSubAtividadeList(subAtividadeList));

        subAtividadeList.addAll(Arrays.asList(sub01, sub02));

        List<Atividade> atividadesOrientacaoPesquisa = this.instanciarAtividadesOrientacaoPesquisa(catAtvOrientacaoGrupoPesquisa, atividadePitList, plan01);
        catAtvOrientacaoGrupoPesquisa.setAtividadeModelList(atividadesOrientacaoPesquisa);

        List<Atividade> atividadesOrientacaoExtensao = this.instanciarAtividadesOrientacaoExtensao(catAtvOrientacaoGrupoExtensao, atividadePitList, plan01);
        catAtvOrientacaoGrupoExtensao.setAtividadeModelList(atividadesOrientacaoExtensao);

        List<Atividade> atividadesOrientacaoGestao = this.instanciarAtividadesOrientacaoGestao(catAtvOrientacaoGrupoGestao, atividadePitList, plan01);
        catAtvOrientacaoGrupoGestao.setAtividadeModelList(atividadesOrientacaoGestao);

        // Instancias as atividades FIM //

        doct02.getPlanosDeTrabalho().add(plan01);

        var env01 = new Envio(null, new Date(), null, plan01);

        plan01.getAtividadePitList().addAll(atividadePitList);
        plan01.getEnvios().add(env01);

        dep01.getCursos().addAll(Arrays.asList(curso01, curso02, curso03));

        dep02.getServidores().add(doct02);
        dep03.getServidores().add(doct03);
        dep01.getServidores().addAll(Arrays.asList(doct01, doct04));

        dep02.getCursos().addAll(Arrays.asList(curso04, curso05));
        dep03.getCursos().add(curso06);

//        // Instancias de Avaliador
        var ava01 = new Avaliador("Avaliação em observação", doct01, camp01);
        var ava02 = new Avaliador("Avaliação em observação", doct02, camp01);
        var ava03 = new Avaliador("Avaliação em observação", doct03, camp02);
        var ava04 = new Avaliador("Avaliação em observação", doct02, camp03);
        var ava05 = new Avaliador("Avaliação em observação", doct04, camp04);


        this.campusRepositorio.saveAll(Arrays.asList(camp01,camp02,camp03,camp04));
        this.departamentoRepositorio.saveAll(Arrays.asList(dep01, dep02, dep03));
        this.cursoRepositorio.saveAll(Arrays.asList(curso01, curso02, curso03, curso04, curso05, curso06));
        this.usuarioRepositorio.saveAll(Arrays.asList(usuario01, usuario02, usuario03));
        this.servidorRepositorio.saveAll(Arrays.asList(doct01, doct02, doct03, doct04));
        this.avaliadorRepositorio.saveAll(Arrays.asList(ava01, ava02, ava03, ava04, ava05));
        this.disciplinaRepositorio.saveAll(Arrays.asList(disc01, disc02, disc03));
        this.grupoAtividadeRepositorio.saveAll(Arrays.asList(grupAtvPesquisa, grupAtvDeEnsino, grupAtvExtensao, grupAtvGestao));

        this.categoriaAtividadeRepositorio.saveAll(Arrays.asList(
                catAtvOrientacaoGrupoGestao,
                catAtvOrientacaoGrupoExtensao,
                catAtvOrientacaoGrupoPesquisa,
                catAtvOrientacaoGrupoEnsino,
                catAtvAulasGrupoEnsino,
                catAtvApoioEnsinoGrupoEnsino,
                catAtvPreparacaoGrupoEnsino));

        this.planoDeTrabalhoRepositorio.save(plan01);
        this.envioRepositorio.save(env01);
        this.atividadeRepositorio.saveAll(subAtividadeList);
        this.atividadeRepositorio.saveAll(atividadesEnsino);
        this.atividadeRepositorio.saveAll(atividadesOrientacao);
        this.atividadeRepositorio.saveAll(atividadesOrientacaoExtensao);
        this.atividadeRepositorio.saveAll(atividadesOrientacaoPesquisa);
        this.atividadeRepositorio.saveAll(atividadesOrientacaoGestao);
        this.atividadePitRepositorio.saveAll(atividadePitList);

    }

    private List<Atividade> instanciarAtividadesOrientacaoGestao(CategoriaAtividade categoriaAtividade, ArrayList<AtividadePit> atividadePitList, PlanoIndividualDeTrabalho planoIndividualDeTrabalho) {
        final var dica = "Dica Preenchimento";

        var atv01 = new Atividade(categoriaAtividade,
                "Coordenação de projetos de extensão que esteja vinculado a um ou mais convênios ou acordos de cooperação interinstitucionais",
                30.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv02 = new Atividade(categoriaAtividade, "Coordenação de Cursos Presenciais",
                30.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv03 = new Atividade(categoriaAtividade, "Órgãos, Núcleos, Conselhos e Colegiados definidos no organograma da Reitoria ou dos campi do IFMT",
                8.0, dica, 2.5, UnidadeAtividade.HORAS, true);

        var atv04 = new Atividade(categoriaAtividade, "Comissões e Comitês permanentes ",
                8.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv10 = new Atividade(categoriaAtividade, "Comissões e Comitês Eventuais",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv05 = new Atividade(categoriaAtividade, "Representação Institucional Externa",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv06 = new Atividade(categoriaAtividade, "Participação no Núcleo Permanente de Pessoal Docente - NPPD",
                6.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv07 = new Atividade(categoriaAtividade, "Participação na Comissão Permanente de Pessoal Docente - CPPD",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv08 = new Atividade(categoriaAtividade, "Coordenações ou tutoria (laboratórios, setores, unidades de produção e afins) – funções não gratificadas",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv09 = new Atividade(categoriaAtividade, "Coordenação e/ou participação de projetos de ensino",
                6.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atvPitOrientacao01 = new AtividadePit(null, 5.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao02 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao03 = new AtividadePit(null, 6.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao04 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv03);
        var atvPitOrientacao05 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv04);
        var atvPitOrientacao06 = new AtividadePit(null, 1.0, planoIndividualDeTrabalho, atv05);

        atividadePitList.addAll(Arrays.asList(atvPitOrientacao01, atvPitOrientacao02, atvPitOrientacao03, atvPitOrientacao04, atvPitOrientacao05, atvPitOrientacao06));

        return new ArrayList<>(Arrays.asList(atv01, atv02, atv03, atv04, atv05, atv06, atv07, atv08, atv09, atv10));
    }

    private List<Atividade> instanciarAtividadesOrientacaoExtensao(CategoriaAtividade categoriaAtividade, ArrayList<AtividadePit> atividadePitList, PlanoIndividualDeTrabalho planoIndividualDeTrabalho) {
        final var dica = "Dica Preenchimento";

        var atv01 = new Atividade(categoriaAtividade,
                "Coordenação de projetos de extensão que esteja vinculado a um ou mais convênios ou acordos de cooperação interinstitucionais",
                8.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv02 = new Atividade(categoriaAtividade, "Participação em projetos de extensão que esteja vinculado a um ou mais convênios ou acordos de cooperação interinstitucionais",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv03 = new Atividade(categoriaAtividade, "Coordenação de projeto de extensão por edital de ampla concorrência no âmbito do IFMT",
                6.0, dica, 2.5, UnidadeAtividade.HORAS, true);

        var atv04 = new Atividade(categoriaAtividade, "Coordenação de projeto de extensão aprovado em Edital Interno ou autorizado pelo campus",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv05 = new Atividade(categoriaAtividade, "Participação em projeto de extensão aprovado em Edital Interno ou autorizado pelo campus",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv06 = new Atividade(categoriaAtividade, "Coordenação e participação na organização de eventos culturais, artísticos, esportivos e comunitários",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv07 = new Atividade(categoriaAtividade, "Participação em treinamentos de equipes desportivas, em competições e em atividades artísticas e culturais.",
                3.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv08 = new Atividade(categoriaAtividade, "Coordenação/acompanhamento Institucional a núcleos empreendedores, programas, cooperativas, empresas juniores, incubadoras, coletivos, agremiações e equipes de estudantes.",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atvPitOrientacao01 = new AtividadePit(null, 5.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao02 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao03 = new AtividadePit(null, 6.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao04 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv03);
        var atvPitOrientacao05 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv04);
        var atvPitOrientacao06 = new AtividadePit(null, 1.0, planoIndividualDeTrabalho, atv05);

        atividadePitList.addAll(Arrays.asList(atvPitOrientacao01, atvPitOrientacao02, atvPitOrientacao03, atvPitOrientacao04, atvPitOrientacao05, atvPitOrientacao06));

        return new ArrayList<>(Arrays.asList(atv01, atv02, atv03, atv04, atv05, atv06, atv07, atv08));
    }

    private List<Atividade> instanciarAtividadesOrientacaoPesquisa(CategoriaAtividade categoriaAtividade, ArrayList<AtividadePit> atividadePitList, PlanoIndividualDeTrabalho planoIndividualDeTrabalho) {
        final var dica = "Dica Preenchimento";

        var atv01 = new Atividade(categoriaAtividade,
                "Coordenação de projeto de pesquisa com parceria externa oficialmente institucionalizado",
                8.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv02 = new Atividade(categoriaAtividade,
                "Coordenação de projeto de pesquisa aprovado em Edital interno ou autorizado pelo campus",
                8.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv03 = new Atividade(categoriaAtividade,
                "Participação/colaboração em pesquisa com parceria externa oficialmente institucionalizada",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv04 = new Atividade(categoriaAtividade,
                "Participação/colaboração em pesquisa aprovada em Edital interno ou autorizado pelo campus",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv05 = new Atividade(categoriaAtividade,
                "Líder de Grupo de Pesquisa com status ativo no CNPq",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv06 = new Atividade(categoriaAtividade,
                "Participação em Grupo de Pesquisa com status ativo no CNPq",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv07 = new Atividade(categoriaAtividade,
                "Editor de revista científica/acadêmica ",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv08 = new Atividade(categoriaAtividade,
                "Participação em banca de defesa de Trabalho de Conclusão de Curso ou monografia",
                0.1, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv09 = new Atividade(categoriaAtividade,
                "Participação em banca de qualificação/defesa de dissertação ou tese do IFMT ou em outra instituição de ensino",
                0.5, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv10 = new Atividade(categoriaAtividade,
                "Elaboração de projetos para captação de recursos financeiros externos ao IFMT ",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv11 = new Atividade(categoriaAtividade,
                "Elaboração de projetos para captação de bolsa produtividade ou desenvolvimento tecnológico do CNPq ",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv12 = new Atividade(categoriaAtividade,
                "Elaboração de pedido de depósito de propriedade intelectual",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv13 = new Atividade(categoriaAtividade,
                "Redação de Patente de inovação tecnológica",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv14 = new Atividade(categoriaAtividade,
                "Produção científica (a convite ou a ser avaliada) destinada a congressos, jornadas científicas, worshops , simpósios, seminários ou periódicos",
                4.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv15 = new Atividade(categoriaAtividade,
                "Participação como apresentador, moderador, debatedor, coordenador, secretário ou palestrante em congressos, jornadas científicas, workshops, simpósios, seminários e outros eventos técnico científicos",
                1.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv16 = new Atividade(categoriaAtividade,
                "Produção de livro técnico ou científico, editoração, organização e/ou tradução de livros técnico-científicos",
                2.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv17 = new Atividade(categoriaAtividade,
                "Participação em comitê ou comissão científica, parecerista e/ou revisor de trabalhos científicos e/ou Eventos",
                1.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv18 = new Atividade(categoriaAtividade,
                "Organização de eventos ligados à pesquisa, à inovação ou à Pós-Graduação ",
                1.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atv19 = new Atividade(categoriaAtividade,
                "Membro de conselho científico, corpo editorial em revistas científicas, consultor adhoc",
                1.0, dica, 1.0, UnidadeAtividade.HORAS, true);

        var atvPitOrientacao01 = new AtividadePit(null, 5.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao02 = new AtividadePit(null, 7.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao03 = new AtividadePit(null, 6.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao04 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv03);
        var atvPitOrientacao05 = new AtividadePit(null, 5.0, planoIndividualDeTrabalho, atv04);
        var atvPitOrientacao06 = new AtividadePit(null, 1.0, planoIndividualDeTrabalho, atv05);

        atividadePitList.addAll(Arrays.asList(atvPitOrientacao01, atvPitOrientacao02, atvPitOrientacao03, atvPitOrientacao04, atvPitOrientacao05, atvPitOrientacao06));

        return new ArrayList<>(Arrays.asList(atv01, atv02, atv03, atv04, atv05, atv06, atv07, atv08, atv09, atv10, atv11, atv12, atv13, atv14, atv15, atv16, atv17, atv18, atv19));
    }

    private List<Atividade> instanciarAtividadesOrientacao(CategoriaAtividade categoriaAtividade, ArrayList<AtividadePit> atividadePitList, PlanoIndividualDeTrabalho planoIndividualDeTrabalho) {
        var atv01 = new Atividade(categoriaAtividade,
                "Orientação de Estágio e monitoria, devidamente caracterizados nos projetos de cursos técnicos e de graduação",
                5.0, "1 hora por estudante", 1.0, UnidadeAtividade.HORAS, false);

        var atv02 = new Atividade(categoriaAtividade, "Orientação de Trabalho de Conclusão de Curso – TCC – de graduação e de cursos de pós-graduação lato sensu ",
                6.0, "1 hora por estudante", 1.0, UnidadeAtividade.HORAS, false);

        var atv03 = new Atividade(categoriaAtividade, "Orientação de Dissertações e teses, nos cursos de pós-graduação stricto sensu",
                10.0, "2,5 hora por estudante", 2.5, UnidadeAtividade.HORAS, false);

        var atv04 = new Atividade(categoriaAtividade, "Orientação profissional nas dependências de empresas que promovam o regime dual de curso em parceria com o IFMT",
                5.0, "1 hora por estudante", 1.0, UnidadeAtividade.HORAS, false);

        var atv05 = new Atividade(categoriaAtividade, "Para as atividades de co-orientação a estudantes de cursos de Pós Graduação Latu Sensu e Strictu Sensu do IFMT",
                3.0, "1 hora por estudante", 1.0, UnidadeAtividade.HORAS, false);

        var atvPitOrientacao01 = new AtividadePit(null, 5.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao02 = new AtividadePit(null, 7.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao03 = new AtividadePit(null, 6.0, planoIndividualDeTrabalho, atv01);
        var atvPitOrientacao04 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv03);
        var atvPitOrientacao05 = new AtividadePit(null, 5.0, planoIndividualDeTrabalho, atv04);
        var atvPitOrientacao06 = new AtividadePit(null, 1.0, planoIndividualDeTrabalho, atv05);

        atividadePitList.addAll(Arrays.asList(atvPitOrientacao01, atvPitOrientacao02, atvPitOrientacao03, atvPitOrientacao04, atvPitOrientacao05, atvPitOrientacao06));

        return new ArrayList<>(Arrays.asList(atv01, atv02, atv03, atv04, atv05));
    }

    private List<Atividade> instanciarAtividadesEnsino(CategoriaAtividade categoriaAtividade, ArrayList<AtividadePit> atividadePitList, PlanoIndividualDeTrabalho planoIndividualDeTrabalho) {

        var atv01 = new Atividade(categoriaAtividade, "Ensino médio integrado e subsequente ", 24.0,
                "Dica Preenchimento", 0.83, UnidadeAtividade.AULAS, false);
        var atv02 = new Atividade(categoriaAtividade, "Ensino superior licenciatura; bacharelado e tecnólogo", 24.0,
                "Dica Preenchimento", 0.83, UnidadeAtividade.AULAS, false);
        var atv03 = new Atividade(categoriaAtividade, "Curso FIC", 24.0,
                "Dica Preenchimento", 0.83, UnidadeAtividade.AULAS, false);
        var atv04 = new Atividade(categoriaAtividade, "Pós graduação (lato sensu e stricto sensu)", 24.0,
                "Dica Preenchimento", 0.83, UnidadeAtividade.AULAS, false);

        var atvPit01 = new AtividadePit(null, 10.0, planoIndividualDeTrabalho, atv01);
        var atvPit02 = new AtividadePit(null, 7.0, planoIndividualDeTrabalho, atv01);
        var atvPit03 = new AtividadePit(null, 6.0, planoIndividualDeTrabalho, atv01);
        var atvPit04 = new AtividadePit(null, 2.0, planoIndividualDeTrabalho, atv03);
        var atvPit05 = new AtividadePit(null, 10.0, planoIndividualDeTrabalho, atv04);

        atividadePitList.addAll(Arrays.asList(atvPit01, atvPit02, atvPit03, atvPit04, atvPit05));

        return new ArrayList<>(Arrays.asList(atv01, atv02, atv03, atv04));
    }
}
