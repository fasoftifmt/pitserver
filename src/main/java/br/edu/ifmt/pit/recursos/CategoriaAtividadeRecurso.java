package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.CategoriaAtividade;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.CategoriaAtividadeServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/categoriaAtividades")
public class CategoriaAtividadeRecurso{

	@Autowired
	private CategoriaAtividadeServico categoriaAtividadeServico;

	@GetMapping(value = "/buscarTodos")
	public ResponseEntity<List<CategoriaAtividade>> buscarTodos(){
		List<CategoriaAtividade> categorias = categoriaAtividadeServico.buscarTodos();
		return ok().body(categorias);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<CategoriaAtividade> buscar(@PathVariable Integer id){
		var categoriaAtividade = categoriaAtividadeServico.buscar(id);
		return ok().body(categoriaAtividade);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping(value = "/inserir")
	public ResponseEntity<MensagemRetorno> inserir(@RequestBody CategoriaAtividade categoriaAtividade){
		var mensagemRetorno = this.categoriaAtividadeServico.inserir(categoriaAtividade);

		if(mensagemRetorno.isSucesso()){
			return ok().body(mensagemRetorno);
		}else{
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
		}
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping(value = "/pagina")
	public ResponseEntity<Page<CategoriaAtividade>> findPage(
			@RequestParam(value = "descricao", defaultValue = "") String descricao,
			@RequestParam(value = "ordem", defaultValue = "0") Integer ordem,
			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "descricao") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction,
			@RequestParam(value = "page", defaultValue = "0") Integer page) {

	     String descricaoDeconded = URL.decodeParam(descricao);

		Page<CategoriaAtividade> lista = categoriaAtividadeServico.buscaPaginada(descricaoDeconded, ordem, page, linesPerPage, orderBy, direction);
		return ResponseEntity.ok().body(lista);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<MensagemRetorno> excluir(@PathVariable Integer id){
		var msgRetorno = this.categoriaAtividadeServico.excluir(id);
		return ok().body(msgRetorno);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@PutMapping
	public ResponseEntity<MensagemRetorno> editar(@RequestBody CategoriaAtividade categoriaAtividade){
		var msgRetorno = this.categoriaAtividadeServico.editar(categoriaAtividade);

		return ok().body(msgRetorno);
	}

}
