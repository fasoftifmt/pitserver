package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class AnexoPlanoDeTrabalho implements Serializable {

    private static final Long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;
    private String descricao;
    private String tipoArquivo;

    @Column(columnDefinition = "LONGBLOB NOT NULL")
    private byte[] arquivo;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "planoDeTrabalho_id")
    private PlanoIndividualDeTrabalho planoDeTrabalho;

    public AnexoPlanoDeTrabalho() {
    }

    public AnexoPlanoDeTrabalho(String nome, String descricao, String tipoArquivo, byte[] arquivo, PlanoIndividualDeTrabalho planoDeTrabalho) {
        this.nome = nome;
        this.descricao = descricao;
        this.tipoArquivo = tipoArquivo;
        this.arquivo = arquivo;
        this.planoDeTrabalho = planoDeTrabalho;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipoArquivo() {
        return tipoArquivo;
    }

    public void setTipoArquivo(String tipoArquivo) {
        this.tipoArquivo = tipoArquivo;
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    public PlanoIndividualDeTrabalho getPlanoDeTrabalho() {
        return planoDeTrabalho;
    }

    public void setPlanoDeTrabalho(PlanoIndividualDeTrabalho planoDeTrabalho) {
        this.planoDeTrabalho = planoDeTrabalho;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AnexoPlanoDeTrabalho)) return false;

        AnexoPlanoDeTrabalho that = (AnexoPlanoDeTrabalho) o;

        return Objects.equals(id, that.id);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "AnexoPlanoDeTrabalho{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", tipoArquivo='" + tipoArquivo + '\'' +
                ", planoDeTrabalho=" + planoDeTrabalho +
                '}';
    }
}
