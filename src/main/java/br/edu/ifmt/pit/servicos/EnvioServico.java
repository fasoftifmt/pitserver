package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.Envio;
import br.edu.ifmt.pit.repositorio.interfaces.EnvioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EnvioServico {

    @Autowired
    private EnvioRepositorio envioRepositorio;

    public Envio buscar(Long id){
        Optional<Envio> obj = envioRepositorio.findById(id);
        return obj.orElse(null);
    }
}
