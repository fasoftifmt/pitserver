package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.Avaliacao;
import br.edu.ifmt.pit.dominio.Avaliador;
import br.edu.ifmt.pit.dominio.dtos.AvaliadorDTO;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.AvaliadorServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping( value = "/avaliador")
public class AvaliadorRecurso {

	@Autowired
	private AvaliadorServico avaliadorServico;


	@GetMapping( value = "/{id}")
	public ResponseEntity<Avaliador> buscar(@PathVariable Long id){
		var avaliacao = avaliadorServico.buscar(id);
		return ok().body(avaliacao);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping( value = "/inserir")
	public ResponseEntity<MensagemRetorno> inserir( @RequestBody Avaliador avaliador){
		var mensagemRetorno = this.avaliadorServico.inserir(avaliador);
		if( mensagemRetorno.isSucesso() ){
			return ok().body(mensagemRetorno);
		}
		else{
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
		}
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping( value = "/pagina")
	public ResponseEntity<Page<Avaliador>> findPage(
		@RequestParam(value = "descricao", defaultValue = "") String descricao,
		@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
		@RequestParam(value = "orderBy", defaultValue = "descricao") String orderBy,
		@RequestParam(value = "direction", defaultValue = "ASC") String direction,
		@RequestParam(value = "page", defaultValue = "0") Integer page)
	{
		String descricaoDeconded = URL.decodeParam(descricao);
		Page<Avaliador> lista = avaliadorServico.buscaPaginada(descricaoDeconded, page, linesPerPage, orderBy, direction);
		return ResponseEntity.ok().body(lista);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<MensagemRetorno> excluir(@PathVariable Long id){
		var mensagemRetorno = this.avaliadorServico.excluir(id);
		return ok().body(mensagemRetorno);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@PutMapping
	public ResponseEntity<MensagemRetorno> editar( @RequestBody Avaliador avaliador){
		var msgRetorno = this.avaliadorServico.editar(avaliador);
		return ok().body(msgRetorno);
	}

	@GetMapping(value = "/buscarTodos")
	public ResponseEntity<List<Avaliador>> buscarTodos(){
		List<Avaliador> avaliadores = avaliadorServico.buscarTodos();
		return ResponseEntity.ok().body(avaliadores);
	}

}
