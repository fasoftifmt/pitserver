package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.Servidor;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.DepartamentoServico;
import br.edu.ifmt.pit.servicos.ServidorServico;
import br.edu.ifmt.pit.servicos.UsuarioServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/servidores")
public class ServidorRecurso {

    @Autowired
    private ServidorServico servidorServico;

    @Autowired
    private UsuarioServico usuarioServico;

    @GetMapping("/relatorios/servidores")
    public ResponseEntity<byte[]> relatorioServidor(Servidor servidor){
        try {
            byte[] relatorio = servidorServico.relatorioServidor(servidor);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
                    .body(relatorio);
        } catch (Exception ex){

            ex.printStackTrace();

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(null);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Servidor> buscar(@PathVariable Long id){
        var servidor = servidorServico.buscar(id);
        return ResponseEntity.ok().body(servidor);
    }

    @GetMapping(value = "/listarTodos")
    public ResponseEntity<List<Servidor>> listarTodos(){
        List<Servidor> servidores = servidorServico.listarTodos();
        return ResponseEntity.ok().body(servidores);
    }

    @GetMapping(value = "/buscarUsuario/{login}")
    public ResponseEntity<Servidor> getUsuarioPorLogin(@PathVariable String login) {
        var servidor = this.servidorServico.buscarPorLogin(login);
        if (servidor != null ) {
            this.usuarioServico.isIdUsuarioIgualAoDoToken(servidor.getUsuario().getIdUsuario());
        }

        return ResponseEntity.ok().body(servidor);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value ="/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody Servidor servidor){

        System.out.println("========================================");
        System.out.println("Servidor: " + servidor);
        System.out.println("Usuario: " + servidor.getUsuario());
        System.out.println("========================================");

        var mensagemRetorno = this.servidorServico.inserir(servidor);
        if( mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        } else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value="/{id}")
    public ResponseEntity<MensagemRetorno> excluir(@PathVariable Long id){
        var msgRetorno = this.servidorServico.excluir(id);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value="/pagina")
    public ResponseEntity<Page<Servidor>> findPage(
            @RequestParam(value = "nome", defaultValue = "") String nome,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction,
            @RequestParam(value = "page", defaultValue = "0") Integer page)
    {
        String nomeDecoded = URL.decodeParam(nome);
        Page<Servidor> lista = servidorServico.buscaPaginada(nomeDecoded, page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(lista);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping()
    public ResponseEntity<MensagemRetorno> editar(@RequestBody Servidor servidor){
        var mensagemRetorno = this.servidorServico.editar(servidor);
        if( mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        } else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }
}
