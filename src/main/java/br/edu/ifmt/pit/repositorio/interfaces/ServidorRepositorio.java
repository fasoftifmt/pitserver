package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.DepartamentoArea;
import br.edu.ifmt.pit.dominio.Servidor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ServidorRepositorio extends JpaRepository<Servidor, Long> {

    public Servidor getServidorByUsuarioLogin(String userName);

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM Servidor obj WHERE obj.nome LIKE %:nome%")
    Page<Servidor> search(@Param("nome") String nome, Pageable pageRequest);

}
