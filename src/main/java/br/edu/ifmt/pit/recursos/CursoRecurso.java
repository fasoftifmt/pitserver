package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.Curso;
import br.edu.ifmt.pit.dominio.dtos.CursoDTO;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.CursoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cursos")
public class CursoRecurso {

    @Autowired
    private CursoServico cursoServico;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Curso> buscar(@PathVariable Long id){
        var curso = cursoServico.buscar(id);
        return ResponseEntity.ok().body(curso);
    }

    @GetMapping
    public ResponseEntity<List<Curso>> listarTodos(){
        List<Curso> cursos = cursoServico.listarTodos();
        return ResponseEntity.ok().body(cursos);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody CursoDTO curso){
        var mensagemRetorno = this.cursoServico.inserir(curso);

        if(mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        }else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value = "/pagina")
    public ResponseEntity<Page<Curso>> findPage(
            @RequestParam(value = "descricao", defaultValue = "") String descricao,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "descricao") String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction) {

        String descricaoDecoded = URL.decodeParam(descricao);

        Page<Curso> lista = cursoServico.buscaPaginada(descricaoDecoded, page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(lista);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MensagemRetorno> excluir(@PathVariable Long id) {

        var msgRetorno = this.cursoServico.excluir(id);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping
    public ResponseEntity<MensagemRetorno> editar(@RequestBody CursoDTO curso) {

        var msgRetorno = this.cursoServico.editar(curso);

        return ResponseEntity.ok().body(msgRetorno);
    }

}
