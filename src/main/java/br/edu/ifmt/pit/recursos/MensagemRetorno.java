package br.edu.ifmt.pit.recursos;

import java.util.ArrayList;
import java.util.List;

public class MensagemRetorno {

    private static final long serialVersionUID = 1L;
    private boolean sucesso;
    private List<String> msgList = new ArrayList<>();

    public MensagemRetorno() {
    }

    public MensagemRetorno(boolean sucesso, String msg) {
        this.sucesso = sucesso;
        this.msgList.add(msg);
    }

    public boolean isSucesso() {
        return sucesso;
    }

    public void setSucesso(boolean sucesso) {
        this.sucesso = sucesso;
    }

    public List<String> getMsgList() {
        return msgList;
    }

    public void setMsgList(List<String> msgList) {
        this.msgList = msgList;
    }

    public void addMsg(String msg){
        msgList.add(msg);
    }

    @Override
    public String toString() {
        return "MensagemRetorno{" +
                "sucesso=" + sucesso +
                ", msgList=" + msgList +
                '}';
    }
}
