package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface
UsuarioRepositorio extends JpaRepository<Usuario, Long> {

    public Usuario findByLogin(String login);


//    @Transactional(readOnly = true)
//    @Query("SELECT DISTINCT obj FROM Usuario obj WHERE obj.login LIKE %:login%")
//    Page<Usuario> search(@Param("login") String login, Pageable pageRequest);
}
