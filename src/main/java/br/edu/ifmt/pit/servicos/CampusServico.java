package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.Campus;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.CampusRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CampusServico {

    @Autowired
    private CampusRepositorio campusRepositorio;

    public Campus buscar(Integer id) {
        Optional<Campus> obj = campusRepositorio.findById(id);
        return obj.orElse(null);
    }

    private MensagemRetorno isCampusValida(Campus campus) {
        if(campus == null) return new MensagemRetorno(false, "Dados de Campus inválidos");

        var mensagemRetorno = new MensagemRetorno();
        mensagemRetorno.setSucesso(true);
        if(campus.getNome() == null || campus.getNome().isEmpty()) {
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Campus inválido");
        }

        return mensagemRetorno;
    }

    public MensagemRetorno salvar(Campus campus) {
        MensagemRetorno campusValida = this.isCampusValida(campus);

        if(!campusValida.isSucesso()) return campusValida;

        try{

            this.campusRepositorio.save(campus);
            campusValida.setSucesso(true);
            campusValida.addMsg("Sucesso ao salvar o campus");
            return campusValida;
        }catch(Exception e){
            e.printStackTrace();
            campusValida.setSucesso(false);
            campusValida.addMsg(e.getMessage());
            return campusValida;
        }
    }

    public MensagemRetorno excluir(Integer id) {
        try{
            this.campusRepositorio.deleteById(id);
            return new MensagemRetorno(true,"Campus excluído com sucesso");
        }catch(Exception e) {
            return new MensagemRetorno(false,"Não é possível excluir pois Campus está sendo usado."+ e.getMessage());
        }
    }

    public Page<Campus> buscaPaginada(String nome, Integer page, Integer linesPerPage, String orderBy, String direction){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return campusRepositorio.search(nome, pageRequest);
    }

    public List<Campus> listarTodos(){
        return this.campusRepositorio.findAll();
    }




}
