package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.CategoriaAtividade;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.CategoriaAtividadeRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaAtividadeServico {

    @Autowired
    private CategoriaAtividadeRepositorio categoriaAtividadeRepositorio;

    public CategoriaAtividade buscar(Integer id) {
        Optional<CategoriaAtividade> obj = categoriaAtividadeRepositorio.findById(id);
        return obj.orElse(null);
    }

	public List<CategoriaAtividade> buscarTodos() {
		return categoriaAtividadeRepositorio.findAll();
	}

	private boolean isCategoriaAtividadeValida(CategoriaAtividade categoriaAtividade){
		if( categoriaAtividade == null ) return false;
		return categoriaAtividade.getDescricao() != null
				&& !categoriaAtividade.getDescricao().isEmpty()
				&& categoriaAtividade.getOrdem() != null;
	}

	public Page<CategoriaAtividade> buscaPaginada(String descricao, Integer ordem, Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
		return categoriaAtividadeRepositorio.search(descricao, pageRequest);
	}

	public MensagemRetorno excluir(Integer id){
		try{
			this.categoriaAtividadeRepositorio.deleteById(id);
			return new MensagemRetorno(true, "Sucesso ao excluir");
		}
		catch(DataIntegrityViolationException e){
			return new MensagemRetorno(false, "Não é possivel excluir pois este registro está sendo usado");
		}
	}

	public MensagemRetorno editar(CategoriaAtividade categoriaAtividade){
		if( !this.isCategoriaAtividadeValida(categoriaAtividade))
			return new MensagemRetorno(false, "Dados da categoria de atividade são inválidos.");
		try{
			this.categoriaAtividadeRepositorio.save(categoriaAtividade);
			return new MensagemRetorno(true, "Registro Atualizado com sucesso!");
		}
		catch( Exception e){
			e.printStackTrace();
			return new MensagemRetorno(false, e.getMessage());
		}
	}

	public MensagemRetorno inserir(CategoriaAtividade categoriaAtividade){
		if( !this.isCategoriaAtividadeValida(categoriaAtividade))
			return new MensagemRetorno(false, "Dados da categoria de atividade são inválidos.");
		try{
			this.categoriaAtividadeRepositorio.save(categoriaAtividade);
			return new MensagemRetorno(true, "Sucesso ao inserir registro");
		}
		catch(Exception e){
			e.printStackTrace();
			return new MensagemRetorno(false, e.getMessage());
		}
	}

}
