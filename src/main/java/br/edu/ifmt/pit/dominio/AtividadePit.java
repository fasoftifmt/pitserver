package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class AtividadePit implements Serializable {

    private static final Long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Double quantidade;
    private Double qtdLancamento;
    private Double fator;
    private String turma;
    private String aulas;

    @ManyToOne
    @JoinColumn(name = "atividade_id")
    private Atividade atividade;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "planoDeTrabalho_id")
    private PlanoIndividualDeTrabalho planoDeTrabalho;

    public AtividadePit() {}

    public AtividadePit(Integer id, Double quantidade, PlanoIndividualDeTrabalho planoDeTrabalho, Atividade atividade) {
        this.quantidade = quantidade;
        this.id = id;
        this.planoDeTrabalho = planoDeTrabalho;
        this.atividade = atividade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Atividade getAtividade() {
        return atividade;
    }

    public void setAtividade(Atividade atividade) {
        this.atividade = atividade;
    }

    public PlanoIndividualDeTrabalho getPlanoDeTrabalho() {
        return planoDeTrabalho;
    }

    public void setPlanoDeTrabalho(PlanoIndividualDeTrabalho planoDeTrabalho) {
        this.planoDeTrabalho = planoDeTrabalho;
    }

    public Double getQtdLancamento() {
        return qtdLancamento;
    }

    public void setQtdLancamento(Double qtdLancamento) {
        this.qtdLancamento = qtdLancamento;
    }

    public Double getFator() {
        return fator;
    }

    public void setFator(Double fator) {
        this.fator = fator;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getAulas() {
        return aulas;
    }

    public void setAulas(String aulas) {
        this.aulas = aulas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AtividadePit atividadePit = (AtividadePit) o;
        return Objects.equals(id, atividadePit.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
