package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.Usuario;
import br.edu.ifmt.pit.dominio.dtos.UsuarioDTO;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.UsuarioServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/usuarios")
@RestController
public class UsuarioRecurso {

    @Autowired
    private UsuarioServico usuarioServico;

    @GetMapping(value = "/listarTodos")
    public ResponseEntity<List<Usuario>> listarTodos(){
        List<Usuario> usuarios = usuarioServico.listarTodos();
        return ResponseEntity.ok().body(usuarios);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Usuario> buscar(@PathVariable Long id){
        var usuario = usuarioServico.buscar(id);
        return ResponseEntity.ok().body(usuario);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value="/buscarUsuario/{login}")
    public ResponseEntity<Usuario> getUsuarioByLogin(@PathVariable String login){
        var usuario = usuarioServico.getUsuarioByLogin(login);
        return ResponseEntity.ok().body(usuario);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/inserir")
    public ResponseEntity <MensagemRetorno> inserir(@RequestBody Usuario usuario){
        var mensagemRetorno =  this.usuarioServico.inserir(usuario);

        if(mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        }else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity <MensagemRetorno> excluir(@PathVariable Long id){
        var mensagemRetorno = this.usuarioServico.excluir(id);

        return ResponseEntity.ok().body(mensagemRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping
    public ResponseEntity <MensagemRetorno> editar(@RequestBody Usuario usuario) {
        var mensagemRetorno = this.usuarioServico.editar(usuario);

        return ResponseEntity.ok().body(mensagemRetorno);
    }

//    @GetMapping(value = "/pagina")
//    public ResponseEntity <Page<Usuario>> findPage(
//            @RequestParam(value = "login", defaultValue = "") String login,
//            @RequestParam(value = "page", defaultValue = "0") Integer page,
//            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
//            @RequestParam(value = "orderBy", defaultValue = "login") String orderBy,
//            @RequestParam(value = "direction", defaultValue = "ASC") String direction){
//        String usuarioDeconded = URL.decodeParam(login);
//
//        Page<Usuario> lista = usuarioServico.buscaPaginada(usuarioDeconded, page, linesPerPage, orderBy, direction);
//
//        return ResponseEntity.ok().body(lista);
//    }
}
