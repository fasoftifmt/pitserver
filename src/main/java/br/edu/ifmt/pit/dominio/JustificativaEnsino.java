package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class JustificativaEnsino implements Serializable {

    private static final Long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String turma;
    private String aulas;

    @OneToOne
    @JoinColumn(name = "disciplina_id")
    private Disciplina disciplina;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "planoDeTrabalho_id")
    private PlanoIndividualDeTrabalho planoDeTrabalho;

    public JustificativaEnsino() {}

    public JustificativaEnsino(String turma, String aulas, Disciplina disciplina, PlanoIndividualDeTrabalho planoDeTrabalho) {
        this.turma = turma;
        this.aulas = aulas;
        this.disciplina = disciplina;
        this.planoDeTrabalho = planoDeTrabalho;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public PlanoIndividualDeTrabalho getPlanoDeTrabalho() {
        return planoDeTrabalho;
    }

    public void setPlanoDeTrabalho(PlanoIndividualDeTrabalho planoDeTrabalho) {
        this.planoDeTrabalho = planoDeTrabalho;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getAulas() {
        return aulas;
    }

    public void setAulas(String aulas) {
        this.aulas = aulas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JustificativaEnsino atividadePit = (JustificativaEnsino) o;
        return Objects.equals(id, atividadePit.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
