package br.edu.ifmt.pit.dominio;

import br.edu.ifmt.pit.dominio.enums.PerfilUsuario;
import br.edu.ifmt.pit.dominio.enums.SituacaoServidor;
import br.edu.ifmt.pit.dominio.enums.RegimeTrabalho;
import br.edu.ifmt.pit.dominio.enums.TipoDeVinculo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
public class Servidor implements Serializable {

    private static final long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idServidor;
    private String nome;
    private String email;
    private String matriculaSiape;
    private String fone;


    @ManyToOne
    @JsonIgnoreProperties("servidores")
    @JoinColumn(name = "departamento_id")
    private DepartamentoArea departamentoArea;

    @OneToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    @Enumerated(EnumType.STRING)
    private RegimeTrabalho regimeTrabalho;

    @Enumerated(EnumType.STRING)
    private TipoDeVinculo tipoDeVinculo;

    @Enumerated(EnumType.STRING)
    private SituacaoServidor situacaoServidor;

    @JsonIgnore
    @OneToMany(mappedBy = "servidor")
    private List<PlanoIndividualDeTrabalho> planosDeTrabalho = new ArrayList<>();

    public Servidor(String nome, String email, String matriculaSiape, String fone, DepartamentoArea departamentoArea,
                    RegimeTrabalho regimeTrabalho, TipoDeVinculo tipoDeVinculo, SituacaoServidor situacaoServidor){

        this.nome = nome;
        this.email = email;
        this.matriculaSiape = matriculaSiape;
        this.fone = fone;
        this.departamentoArea = departamentoArea;
        this.regimeTrabalho = regimeTrabalho;
        this.tipoDeVinculo = tipoDeVinculo;
        this.situacaoServidor = situacaoServidor;
    }

    public Servidor() {
    }

    public Long getIdServidor() {
        return idServidor;
    }

    public void setIdServidor(Long idServidor) {
        this.idServidor = idServidor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMatriculaSiape() {
        return matriculaSiape;
    }

    public void setMatriculaSiape(String matriculaSiape) {
        this.matriculaSiape = matriculaSiape;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public DepartamentoArea getDepartamentoArea() {
        return departamentoArea;
    }

    public void setDepartamentoArea(DepartamentoArea departamentoArea) {
        this.departamentoArea = departamentoArea;
    }

    public RegimeTrabalho getRegimeTrabalho() {
        return regimeTrabalho;
    }

    public void setRegimeTrabalho(RegimeTrabalho regimeTrabalho) {
        this.regimeTrabalho = regimeTrabalho;
    }

    public List<PlanoIndividualDeTrabalho> getPlanosDeTrabalho() {
        return planosDeTrabalho;
    }

    public void setPlanosDeTrabalho(List<PlanoIndividualDeTrabalho> planosDeTrabalho) {
        this.planosDeTrabalho = planosDeTrabalho;
    }

    public TipoDeVinculo getTipoDeVinculo() {
        return tipoDeVinculo;
    }

    public void setTipoDeVinculo(TipoDeVinculo tipoDeVinculo) {
        this.tipoDeVinculo = tipoDeVinculo;
    }

    public SituacaoServidor getSituacaoServidor() {
        return situacaoServidor;
    }

    public void setSituacaoServidor(SituacaoServidor situacaoServidor) {
        this.situacaoServidor = situacaoServidor;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Servidor servidor = (Servidor) o;
        return Objects.equals(idServidor, servidor.idServidor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idServidor);
    }

    @Override
    public String toString() {
        return "Servidor{" +
                "idServidor=" + idServidor +
                ", nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", matriculaSiape='" + matriculaSiape + '\'' +
                ", fone='" + fone + '\'' +
                ", departamentoArea=" + departamentoArea +
                ", usuario=" + usuario +
                ", regimeTrabalho=" + regimeTrabalho +
                ", tipoDeVinculo=" + tipoDeVinculo +
                ", situacaoServidor=" + situacaoServidor +
                ", planosDeTrabalho=" + planosDeTrabalho +
                '}';
    }
}

