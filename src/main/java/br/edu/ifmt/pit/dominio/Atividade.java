package br.edu.ifmt.pit.dominio;

import br.edu.ifmt.pit.dominio.enums.UnidadeAtividade;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Atividade implements Serializable {

    private static final Long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double fator;
    private String descricao;
    private String dicaPreenchimento;

    @Enumerated(EnumType.STRING)
    private UnidadeAtividade unidadeAtividade;

    @ManyToOne
    @JoinColumn(name = "categoria_id")
    private CategoriaAtividade categoria;

    @JsonIgnore
    @OneToMany(mappedBy = "atividade")
    private List<AtividadePit> atividadePitList = new ArrayList<>();

    @JsonIgnoreProperties("categoria")
    @ManyToMany
    @JoinTable(name = "ATIVIDADE_SUB_ATIVIDADE",
            joinColumns = @JoinColumn(name = "atividade_id"),
            inverseJoinColumns = @JoinColumn(name = "sub_atividade_id")
    )
    private List<Atividade> subAtividadeList = new ArrayList<>();

    private boolean isSubAtividade;

    private Double qtdMax;
    private boolean permiteLancamentoMultiplos;

    public Atividade() {
    }

    public Atividade(CategoriaAtividade categoria, String descricao, Double qtdMax, String dicaPreenchimento,
                     Double fator, UnidadeAtividade unidadeAtividade, boolean permiteLancamentoMultiplos) {
        this.qtdMax = qtdMax;
        this.categoria = categoria;
        this.descricao = descricao;
        this.dicaPreenchimento = dicaPreenchimento;
        this.fator = fator;
        this.unidadeAtividade = unidadeAtividade;
        this.permiteLancamentoMultiplos = permiteLancamentoMultiplos;
    }

    public List<AtividadePit> getAtividadePitList() {
        return atividadePitList;
    }

    public void setAtividadePitList(List<AtividadePit> atividadePitList) {
        this.atividadePitList = atividadePitList;
    }

    public Double getQtdMax() {
        return qtdMax;
    }

    public void setQtdMax(Double qtdMax) {
        this.qtdMax = qtdMax;
    }

    public boolean isPermiteLancamentoMultiplos() {
        return permiteLancamentoMultiplos;
    }

    public void setPermiteLancamentoMultiplos(boolean permiteLancamentoMultiplos) {
        this.permiteLancamentoMultiplos = permiteLancamentoMultiplos;
    }

    public List<Atividade> getSubAtividadeList() {
        return subAtividadeList;
    }

    public void setSubAtividadeList(List<Atividade> subAtividadeList) {
        this.subAtividadeList = subAtividadeList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFator() {
        return fator;
    }

    public void setFator(Double fator) {
        this.fator = fator;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDicaPreenchimento() {
        return dicaPreenchimento;
    }

    public void setDicaPreenchimento(String dicaPreenchimento) {
        this.dicaPreenchimento = dicaPreenchimento;
    }

    public UnidadeAtividade getUnidadeAtividade() {
        return unidadeAtividade;
    }

    public void setUnidadeAtividade(UnidadeAtividade unidadeAtividade) {
        this.unidadeAtividade = unidadeAtividade;
    }

    public CategoriaAtividade getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaAtividade categoria) {
        this.categoria = categoria;
    }

    public boolean isSubAtividade() {
        return isSubAtividade;
    }

    public void setSubAtividade(boolean isSubAtividade) {
        this.isSubAtividade = isSubAtividade;
    }
}
