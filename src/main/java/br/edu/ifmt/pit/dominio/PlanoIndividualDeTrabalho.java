package br.edu.ifmt.pit.dominio;

import br.edu.ifmt.pit.dominio.enums.StatusPit;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class PlanoIndividualDeTrabalho implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-4")
    private Date dataElaboracao;

    @Enumerated(EnumType.STRING)
    private StatusPit statusPtd;

    @OneToMany(mappedBy = "planoDeTrabalho")
    private List<AtividadePit> atividadePitList = new ArrayList<>();

    @OneToMany(mappedBy = "planoDeTrabalho")
    private List<JustificativaEnsino> justificativaEnsinoList = new ArrayList<>();

    @OneToMany(mappedBy = "planoDeTrabalho")
    private List<AnexoPlanoDeTrabalho> anexoPITList = new ArrayList<>();

    @OneToMany(mappedBy = "planoDeTrabalho")
    private List<Envio> envios = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "docente_id")
    private Servidor servidor;

    public PlanoIndividualDeTrabalho() {}

    public PlanoIndividualDeTrabalho(Long id, Date dataElaboracao, StatusPit statusPtd, Servidor servidor) {
        this.dataElaboracao = dataElaboracao;
        this.statusPtd = statusPtd;
        this.servidor = servidor;
        this.id = id;
    }

    public List<AtividadePit> getAtividadePitList() {
        return atividadePitList;
    }

    public void setAtividadePitList(List<AtividadePit> atividadePitList) {
        this.atividadePitList = atividadePitList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataElaboracao() {
        return dataElaboracao;
    }

    public void setDataElaboracao(Date dataElaboracao) {
        this.dataElaboracao = dataElaboracao;
    }

    public StatusPit getStatusPtd() {
        return statusPtd;
    }

    public void setStatusPtd(StatusPit statusPtd) {
        this.statusPtd = statusPtd;
    }

    public Servidor getServidor() {
        return servidor;
    }

    public void setServidor(Servidor docente) {
        this.servidor = docente;
    }

    public List<Envio> getEnvios() {
        return envios;
    }

    public void setEnvios(List<Envio> envios) {
        this.envios = envios;
    }

    public List<JustificativaEnsino> getJustificativaEnsinoList() {
        return justificativaEnsinoList;
    }

    public void setJustificativaEnsinoList(List<JustificativaEnsino> justificativaEnsinoList) {
        this.justificativaEnsinoList = justificativaEnsinoList;
    }

    public List<AnexoPlanoDeTrabalho> getAnexoPITList() {
        return anexoPITList;
    }

    public void setAnexoPITList(List<AnexoPlanoDeTrabalho> anexoPITList) {
        this.anexoPITList = anexoPITList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanoIndividualDeTrabalho that = (PlanoIndividualDeTrabalho) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
