package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.Curso;
import br.edu.ifmt.pit.dominio.PlanoIndividualDeTrabalho;
import br.edu.ifmt.pit.dominio.enums.StatusPit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PlanoDeTrabalhoRepositorio extends JpaRepository<PlanoIndividualDeTrabalho, Long> {

    public List<PlanoIndividualDeTrabalho> findByServidor_IdServidor(Long idServidor);

    public List<PlanoIndividualDeTrabalho> findAllByStatusPtdEquals(StatusPit statusPit);

    @Transactional(readOnly = true)
    @Query("select obj from PlanoIndividualDeTrabalho obj " +
            "join obj.envios en where obj.statusPtd =:statusPit")
    public List<PlanoIndividualDeTrabalho> buscarParaAvaliar(@Param("statusPit") StatusPit statusPit);

//
//    @Transactional(readOnly = true)
//    @Query("SELECT DISTINCT obj FROM Curso obj WHERE obj.descricao LIKE %:descricao%")
//    Page<Curso> search(@Param("descricao") String descricao, Pageable pageRequest);


}
