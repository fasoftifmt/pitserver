package br.edu.ifmt.pit.dominio;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class DepartamentoArea implements Serializable {

    private static final long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String descricao;
    private String sigla;

    @JsonIgnoreProperties("departamentoArea")
    @OneToMany(mappedBy = "departamentoArea")
    private List<Curso> cursos = new ArrayList<>();

//    @OneToMany(mappedBy = "departamentoArea")
//    private Campus campus;

    @JsonIgnoreProperties("departamentoArea")
    @OneToMany(mappedBy = "departamentoArea")
    private List<Servidor> servidores = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "campus_id")
    private Campus campus;

    public DepartamentoArea() {
    }

    public DepartamentoArea(Long id, String descricao, String sigla, Campus campus) {
        this.descricao = descricao;
        this.sigla = sigla;
        this.id = id;
        this.campus = campus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Servidor> getServidores() {
        return servidores;
    }

    public void setServidores(List<Servidor> docentes) {
        this.servidores = docentes;
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartamentoArea that = (DepartamentoArea) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
