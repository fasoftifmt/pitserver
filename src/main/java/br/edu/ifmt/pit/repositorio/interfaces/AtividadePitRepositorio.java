package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.AtividadePit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtividadePitRepositorio extends JpaRepository<AtividadePit, Integer> {

}
