package br.edu.ifmt.pit.dominio.enums;

public enum PerfilUsuario {

    SERVIDOR(1, "ROLE_SERVIDOR"),
    ADMIN(2, "ROLE_ADMIN"),
    GESTOR(3, "ROLE_GESTOR");

    private Integer id;
    private String perfil;

    PerfilUsuario(Integer id, String perfil) {
        this.id = id;
        this.perfil = perfil;
    }

    public Integer getId() {
        return id;
    }

    public String getPerfil() {
        return perfil;
    }

    public static PerfilUsuario toEnum(Integer cod){
        if (cod == null) {
            return null;
        }
        for (PerfilUsuario x: PerfilUsuario.values()) {
            if (cod.equals(x.getId())) {
                return x;
             }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }


    @Override
    public String toString() {
        return this.perfil;
    }
}
