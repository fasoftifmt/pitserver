package br.edu.ifmt.pit.dominio.dtos;

import java.util.Date;

public class RegistroHistoricoDTO {

    private static final long serialVersionID = 1L;

    private Long id;
    private Long pitid;
    private Date dataRegistro;
    private String descricao;
    private String statusPitAnterior;
    private String statusPitAtual;
    private Long autorAcaoID;

    public RegistroHistoricoDTO() {
    }

    public RegistroHistoricoDTO(Long pitid, Date dataRegistro, String descricao, String statusPitAnterior, String statusPitAtual, Long autorAcaoID) {
        this.pitid = pitid;
        this.dataRegistro = dataRegistro;
        this.descricao = descricao;
        this.statusPitAnterior = statusPitAnterior;
        this.statusPitAtual = statusPitAtual;
        this.autorAcaoID = autorAcaoID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPitid() {
        return pitid;
    }

    public void setPitid(Long pitid) {
        this.pitid = pitid;
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatusPitAnterior() {
        return statusPitAnterior;
    }

    public void setStatusPitAnterior(String statusPitAnterior) {
        this.statusPitAnterior = statusPitAnterior;
    }

    public String getStatusPitAtual() {
        return statusPitAtual;
    }

    public void setStatusPitAtual(String statusPitAtual) {
        this.statusPitAtual = statusPitAtual;
    }

    public Long getAutorAcaoID() {
        return autorAcaoID;
    }

    public void setAutorAcaoID(Long autorAcaoID) {
        this.autorAcaoID = autorAcaoID;
    }
}
