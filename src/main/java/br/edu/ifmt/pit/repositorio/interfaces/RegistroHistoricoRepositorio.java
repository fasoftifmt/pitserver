package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.RegistroHistorico;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface RegistroHistoricoRepositorio extends JpaRepository<RegistroHistorico, Long> {
    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM RegistroHistorico obj WHERE obj.descricao LIKE %:descricao%")
    Page<RegistroHistorico> search(@Param("descricao") String descricao, Pageable pageRequest);




}
