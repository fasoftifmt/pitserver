package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.Avaliador;
import br.edu.ifmt.pit.dominio.Envio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AvalicaoRepositorio extends JpaRepository<Envio, Long> {

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM Avaliador obj WHERE obj.descricao LIKE %:descricao%")
    Page<Avaliador> search(@Param("descricao") String descricao, Pageable pageRequest);
}
