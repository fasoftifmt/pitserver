package br.edu.ifmt.pit.dominio.enums;

public enum TipoDeVinculo {

    EFETIVO(0, "Efetivo"),
    TEMPORARIO(1, "Temporario"),
    SUBSTITUTO(2, "Substituto"),
    COLABORACAO_TECNICA(3, "Colaboração Tecnica");

    private int cod;
    private String descricao;

    TipoDeVinculo(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public int getCod() {
        return cod;
    }

    public static TipoDeVinculo toEnum(Integer cod){
        if (cod == null) {
            return null;
        }

        for (TipoDeVinculo x: TipoDeVinculo.values()) {
            if (cod.equals(x.getCod())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

    public String getDescricao() {
        return descricao;
    }
}
