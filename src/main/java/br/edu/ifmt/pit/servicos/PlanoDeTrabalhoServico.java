package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.*;
import br.edu.ifmt.pit.dominio.dtos.AnexoPlanoDeTrabalhoDTO;
import br.edu.ifmt.pit.dominio.dtos.PlanoDeTrabalhoResumidoDTO;
import br.edu.ifmt.pit.dominio.dtos.PlanoIndividualDeTrabalhoDTO;
import br.edu.ifmt.pit.dominio.dtos.RegistroHistoricoDTO;
import br.edu.ifmt.pit.dominio.enums.StatusPit;
import br.edu.ifmt.pit.exceptions.AuthorizationException;
import br.edu.ifmt.pit.repositorio.interfaces.AnexoRepositorio;
import br.edu.ifmt.pit.repositorio.interfaces.PlanoDeTrabalhoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class PlanoDeTrabalhoServico {

    @Autowired
    private PlanoDeTrabalhoRepositorio planoDeTrabalhoRepositorio;
    @Autowired
    private AnexoRepositorio anexoRepositorio;
    @Autowired
    private ServidorServico servidorServico;
    @Autowired
    private AtividadePitServico atividadePitServico;
    @Autowired
    private JustificativaEnsinoServico justificativaEnsinoServico;
    @Autowired
    UsuarioServico usuarioServico;
    @Autowired
    RegistroHistoricoServico registroServico;

    public PlanoIndividualDeTrabalho buscar(Long id) throws AuthorizationException {
        var obj = planoDeTrabalhoRepositorio.findById(id).orElse(null);
        if (obj != null) {
            this.usuarioServico.isIdUsuarioIgualAoDoToken(obj.getServidor().getIdServidor());
        }

        return obj;
    }

    @Transactional
    public PlanoIndividualDeTrabalho insert(PlanoIndividualDeTrabalhoDTO planoDeTrabalhoDTO) {
        PlanoIndividualDeTrabalho planoDeTrabalho = this.fromDTO(planoDeTrabalhoDTO);

        planoDeTrabalho.setId(null);
        planoDeTrabalho.setServidor(this.servidorServico.buscar(planoDeTrabalho.getServidor().getIdServidor()));

        planoDeTrabalho = this.planoDeTrabalhoRepositorio.save(planoDeTrabalho);
        for (AtividadePit atv : planoDeTrabalho.getAtividadePitList()) {
            atv.setPlanoDeTrabalho(planoDeTrabalho);
        }

        for (JustificativaEnsino justificativaEnsino : planoDeTrabalho.getJustificativaEnsinoList()) {
            justificativaEnsino.setPlanoDeTrabalho(planoDeTrabalho);
        }

        this.justificativaEnsinoServico.salvar(planoDeTrabalho.getJustificativaEnsinoList());
        this.atividadePitServico.salvar(planoDeTrabalho.getAtividadePitList());
        if (planoDeTrabalho.getAnexoPITList() != null && !planoDeTrabalho.getAnexoPITList().isEmpty()) {
            this.anexoRepositorio.saveAll(planoDeTrabalho.getAnexoPITList());
        }

        this.registroServico.inserir(new RegistroHistoricoDTO(planoDeTrabalho.getId(),planoDeTrabalho.getDataElaboracao(),"Registro de inclusao",
                String.valueOf(planoDeTrabalho.getStatusPtd()),String.valueOf(planoDeTrabalho.getStatusPtd()), planoDeTrabalho.getServidor().getIdServidor()));

        return planoDeTrabalho;
    }



    public List<PlanoDeTrabalhoResumidoDTO> buscarPlanosDeTrabalhoPorServidor(Long idServidor) {
        var planosDeTrabalho = this.planoDeTrabalhoRepositorio.findByServidor_IdServidor(idServidor);

        if (!planosDeTrabalho.isEmpty()) {
            var pit = planosDeTrabalho.get(0);
            this.usuarioServico.isIdUsuarioIgualAoDoToken(pit.getServidor().getIdServidor());
        }

        return this.listPlanoDeTrabalhoToDTO(planosDeTrabalho);
    }

    private List<PlanoDeTrabalhoResumidoDTO> listPlanoDeTrabalhoToDTO(List<PlanoIndividualDeTrabalho> planoDeTrabalhoList) {
        var pitResumidoDTOList = new ArrayList<PlanoDeTrabalhoResumidoDTO>();
        planoDeTrabalhoList.forEach(pit -> pitResumidoDTOList.add(this.planoDeTrabalhoToDTO(pit)));

        return pitResumidoDTOList;
    }

    private PlanoDeTrabalhoResumidoDTO planoDeTrabalhoToDTO(PlanoIndividualDeTrabalho pit) {
        return new PlanoDeTrabalhoResumidoDTO(pit.getId(), pit.getDataElaboracao(), pit.getStatusPtd());
    }

    private PlanoIndividualDeTrabalho fromDTO(PlanoIndividualDeTrabalhoDTO pitDTO) {
        PlanoIndividualDeTrabalho pit = new PlanoIndividualDeTrabalho();

        pit.setId(pitDTO.getId());
        pit.setServidor(pitDTO.getServidor());
        pit.setAtividadePitList(pitDTO.getAtividadePitList());
        pit.setDataElaboracao(pitDTO.getDataElaboracao());
        pit.setEnvios(pitDTO.getEnvios());
        pit.setStatusPtd(pitDTO.getStatusPtd());
        pit.setJustificativaEnsinoList(pitDTO.getJustificativaEnsinoList());
        var anexos = this.listaAnexoFromDTO(pitDTO.getAnexoPITList());
        anexos.forEach(anexo -> anexo.setPlanoDeTrabalho(pit));
        pit.setAnexoPITList(anexos);

        return pit;
    }

    private List<AnexoPlanoDeTrabalho> listaAnexoFromDTO(List<AnexoPlanoDeTrabalhoDTO> anexoDTOList) {

        if (anexoDTOList == null || anexoDTOList.isEmpty()) {
            return Collections.emptyList();
        }
        var anexoList = new ArrayList<AnexoPlanoDeTrabalho>();

        anexoDTOList.forEach(anexoDTO -> anexoList.add(this.anexoFromDTO(anexoDTO)));

        return anexoList;

    }

    private AnexoPlanoDeTrabalho anexoFromDTO(AnexoPlanoDeTrabalhoDTO anexoDTO) {
        var anexo = new AnexoPlanoDeTrabalho();

        anexo.setDescricao(anexoDTO.getDescricao());
        anexo.setId(anexoDTO.getId());
        anexo.setNome(anexoDTO.getNome());
        anexo.setTipoArquivo(anexoDTO.getTipo());
        anexo.setArquivo(Base64.getDecoder().decode(anexoDTO.getDocBase64()));

        return anexo;
    }

    public List<PlanoDeTrabalhoResumidoDTO> buscarPlanosDeTrabalhoParaAvaliar(Long idServidor) {
        var pitParaAvaliarList = this.planoDeTrabalhoRepositorio.findAllByStatusPtdEquals(StatusPit.ENVIADO);
        return Collections.emptyList();
    }
}
