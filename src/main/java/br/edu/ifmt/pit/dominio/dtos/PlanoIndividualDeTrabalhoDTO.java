package br.edu.ifmt.pit.dominio.dtos;

import br.edu.ifmt.pit.dominio.*;
import br.edu.ifmt.pit.dominio.enums.StatusPit;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlanoIndividualDeTrabalhoDTO {


    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-4")
    private Date dataElaboracao;

    private StatusPit statusPtd;

    private List<AtividadePit> atividadePitList = new ArrayList<>();

    private List<JustificativaEnsino> justificativaEnsinoList = new ArrayList<>();

    private List<AnexoPlanoDeTrabalhoDTO> anexoPITList = new ArrayList<>();

    private List<Envio> envios = new ArrayList<>();

    private Servidor servidor;

    public PlanoIndividualDeTrabalhoDTO() {}

    public PlanoIndividualDeTrabalhoDTO(Long id, Date dataElaboracao, StatusPit statusPtd, Servidor servidor) {
        this.dataElaboracao = dataElaboracao;
        this.statusPtd = statusPtd;
        this.servidor = servidor;
        this.id = id;
    }

    public List<AtividadePit> getAtividadePitList() {
        return atividadePitList;
    }

    public void setAtividadePitList(List<AtividadePit> atividadePitList) {
        this.atividadePitList = atividadePitList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataElaboracao() {
        return dataElaboracao;
    }

    public void setDataElaboracao(Date dataElaboracao) {
        this.dataElaboracao = dataElaboracao;
    }

    public StatusPit getStatusPtd() {
        return statusPtd;
    }

    public void setStatusPtd(StatusPit statusPtd) {
        this.statusPtd = statusPtd;
    }

    public Servidor getServidor() {
        return servidor;
    }

    public void setServidor(Servidor docente) {
        this.servidor = docente;
    }

    public List<Envio> getEnvios() {
        return envios;
    }

    public void setEnvios(List<Envio> envios) {
        this.envios = envios;
    }

    public List<JustificativaEnsino> getJustificativaEnsinoList() {
        return justificativaEnsinoList;
    }

    public void setJustificativaEnsinoList(List<JustificativaEnsino> justificativaEnsinoList) {
        this.justificativaEnsinoList = justificativaEnsinoList;
    }

    public List<AnexoPlanoDeTrabalhoDTO> getAnexoPITList() {
        return anexoPITList;
    }

    public void setAnexoPITList(List<AnexoPlanoDeTrabalhoDTO> anexoPITList) {
        this.anexoPITList = anexoPITList;
    }

}
