package br.edu.ifmt.pit.dominio;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Avaliador implements Serializable {

    private static final Long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAvaliador;

    @OneToOne
    @JoinColumn(name = "servidor_id")
    private Servidor servidor;

    @Column(length = 100)
    private String descricao;

    @ManyToOne
    @JoinColumn(name = "campus_id")
    private Campus campus;

    public Avaliador() {
    }

    public Avaliador(Long idAvaliador, String descricao, Servidor servidor, Campus campus) {
        this.idAvaliador = idAvaliador;
        this.servidor = servidor;
        this.descricao = descricao;
        this.campus = campus;
    }
    public Avaliador(String descricao, Servidor servidor, Campus campus) {
        this.idAvaliador = idAvaliador;
        this.servidor = servidor;
        this.descricao = descricao;
        this.campus = campus;
    }

    public Long getIdAvaliador() {
        return idAvaliador;
    }

    public void setIdAvaliador(Long idAvaliador) {
        this.idAvaliador = idAvaliador;
    }

    public Servidor getServidor() {
        return servidor;
    }

    public void setServidor(Servidor servidor) {
        this.servidor = servidor;
    }

    public void setDescricao(String descricao){this.descricao = descricao;}

    public String getDescricao() {
        return descricao;
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Avaliador)) return false;

        Avaliador that = (Avaliador) o;

        return Objects.equals(idAvaliador, that.idAvaliador);
    }

    @Override
    public int hashCode() {
        return idAvaliador != null ? idAvaliador.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Avaliador{" +
                "idAvaliador=" + idAvaliador +
                ", servidor=" + servidor +
                ", campus=" + campus +
                '}';
    }
}
