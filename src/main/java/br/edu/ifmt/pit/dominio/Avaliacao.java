package br.edu.ifmt.pit.dominio;

import br.edu.ifmt.pit.dominio.enums.Situacao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class Avaliacao implements Serializable {

    private static final Long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAvaliacao;

    @Column(length = 100)
    private String descricao;

    private Date dataTransicao;

    @Enumerated(EnumType.STRING)
    private Situacao situacao;

    @OneToOne
    @JoinColumn(name = "avaliador_id")
    private Avaliador servidor;

    public Avaliacao(String descricao, Date dataTransicao, Situacao situacao) {
        this.descricao = descricao;
        this.dataTransicao = dataTransicao;
        this.situacao = situacao;
    }

    public Avaliacao() {}

    public Integer getIdAvaliacao() {
        return idAvaliacao;
    }

    public void setIdAvaliacao(Integer idAvaliacao) {
        this.idAvaliacao = idAvaliacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataTransicao() {
        return dataTransicao;
    }

    public void setDataTransicao(Date dataTransicao) {
        this.dataTransicao = dataTransicao;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Avaliacao)) return false;

        Avaliacao avaliacao = (Avaliacao) o;

        return Objects.equals(idAvaliacao, avaliacao.idAvaliacao);
    }

    @Override
    public int hashCode() {
        return idAvaliacao != null ? idAvaliacao.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Avaliacao{" +
                "idAvaliacao=" + idAvaliacao +
                ", descricao='" + descricao + '\'' +
                ", instante=" + dataTransicao +
                ", situacao=" + situacao +
                '}';
    }
}
