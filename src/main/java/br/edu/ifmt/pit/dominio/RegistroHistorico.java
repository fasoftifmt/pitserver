package br.edu.ifmt.pit.dominio;

import br.edu.ifmt.pit.dominio.enums.StatusPit;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class RegistroHistorico implements Serializable {

    private static final long serialVersionID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name="pit_id")
    private PlanoIndividualDeTrabalho pit;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "GMT-4")
    private Date dataRegistro;

    private String descricao;

    @Enumerated(EnumType.STRING)
    private StatusPit statusPitAnterior;

    @Enumerated(EnumType.STRING)
    private StatusPit statusPitAtual;

    @ManyToOne
    @JoinColumn(name = "servidor_id")
    private Servidor autorAcao;


    public RegistroHistorico() {
    }

    public RegistroHistorico(PlanoIndividualDeTrabalho pit, Date dataRegistro, String descricao, StatusPit statusPitAnterior, StatusPit statusPitAtual, Servidor autorAcao) {
        this.pit = pit;
        this.dataRegistro = dataRegistro;
        this.descricao = descricao;
        this.statusPitAnterior = statusPitAnterior;
        this.statusPitAtual = statusPitAtual;
        this.autorAcao = autorAcao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public StatusPit getStatusPitAnterior() {
        return statusPitAnterior;
    }

    public void setStatusPitAnterior(StatusPit statusPitAnterior) {
        this.statusPitAnterior = statusPitAnterior;
    }

    public StatusPit getStatusPitAtual() {
        return statusPitAtual;
    }

    public void setStatusPitAtual(StatusPit statusPitAtual) {
        this.statusPitAtual = statusPitAtual;
    }

    public Servidor getAutorAcao() {
        return autorAcao;
    }

    public void setAutorAcao(Servidor autorAcao) {
        this.autorAcao = autorAcao;
    }

    public PlanoIndividualDeTrabalho getPit() {
        return pit;
    }

    public void setPit(PlanoIndividualDeTrabalho pit) {
        this.pit = pit;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistroHistorico that = (RegistroHistorico) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
