package br.edu.ifmt.pit.servicos;
import br.edu.ifmt.pit.dominio.Curso;
import br.edu.ifmt.pit.dominio.DepartamentoArea;
import br.edu.ifmt.pit.dominio.dtos.CursoDTO;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.CursoRepositorio;
import br.edu.ifmt.pit.repositorio.interfaces.DepartamentoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CursoServico {

    @Autowired
    private CursoRepositorio cursoRepositorio;

    @Autowired
    private DepartamentoRepositorio depRepositorio;

    public Curso buscar(Long id){
        Optional<Curso> obj = cursoRepositorio.findById(id);
        return obj.orElse(null);
    }

    public List<Curso> listarTodos() {
        return this.cursoRepositorio.findAll();
    }

    public MensagemRetorno inserir(CursoDTO cursoDTO){

        Curso curso = this.fromDTO(cursoDTO);
        if(!this.isCursoValido(curso)){
            return new MensagemRetorno(false,"Dados de curso inválidos");
        }

        try {
            this.cursoRepositorio.save(curso);
            return new MensagemRetorno(true,"Sucesso ao inserir registro");
        }catch (Exception e){
            e.printStackTrace();;
            return new MensagemRetorno(false,e.getMessage());
        }
    }

    public boolean isCursoValido(Curso curso){
        if (curso == null) return false;

        return curso.getDescricao() != null && !curso.getDescricao().isEmpty();
    }


    public Page<Curso> buscaPaginada(String descricao, Integer page, Integer linesPerPage,
                                     String orderBy, String direction){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return cursoRepositorio.search(descricao, pageRequest);
    }

    public MensagemRetorno excluir(Long id){

        try {
            this.cursoRepositorio.deleteById(id);
            return new MensagemRetorno(true, "Sucesso ao excluir");
        }catch (DataIntegrityViolationException ex){
            return  new MensagemRetorno(false, "Não é possível excluir este registro pois ele está" +
                    "sendo usado");
        }
    }

    public MensagemRetorno editar(CursoDTO cursoDTO){
        Curso curso = this.fromDTO(cursoDTO);
        if (!this.isCursoValido(curso)){
            return  new MensagemRetorno(false,"Dados de curso inválidos");
        }

        try {
            this.cursoRepositorio.save(curso);
            return new MensagemRetorno( true, "Registro atualizado com sucesso");
        }catch(Exception e){
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }
    }

    public Curso fromDTO(CursoDTO dto){
        Curso curso = new Curso();
        curso.setDescricao(dto.getDescricao());
        Optional<DepartamentoArea> departamento = this.depRepositorio.findById(dto.getDepartamentoID());
        departamento.ifPresent(curso::setDepartamentoArea);

        curso.setId(dto.getId());

        return curso;
    }

}
