package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.DepartamentoArea;
import br.edu.ifmt.pit.dominio.GrupoAtividade;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.DepartamentoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/departamentos")
public class DepartamentoRecurso {

    @Autowired
    private DepartamentoServico departamentoServico;

    @GetMapping(value = "/buscarTodos")
    public ResponseEntity<List<DepartamentoArea>> buscarTodos(){
        List<DepartamentoArea> categorias = departamentoServico.buscarTodos();
        return ResponseEntity.ok().body(categorias);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<DepartamentoArea> buscar(@PathVariable Long id) {
        var departamento = departamentoServico.buscar(id);
        return ResponseEntity.ok().body(departamento);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody DepartamentoArea departamentoArea) {

        var mensagemRetorno = this.departamentoServico.inserir(departamentoArea);

        if (mensagemRetorno.isSucesso()) {
            return ResponseEntity.ok().body(mensagemRetorno);
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value = "/pagina")
    public ResponseEntity<Page<DepartamentoArea>> findPage(
            @RequestParam(value = "descricao", defaultValue = "") String descricao,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "descricao") String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction) {

        String descricaoDeconded = URL.decodeParam(descricao);

        Page<DepartamentoArea> lista = departamentoServico.buscaPaginada(descricaoDeconded, page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(lista);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MensagemRetorno> excluir(@PathVariable Long id) {

        var msgRetorno = this.departamentoServico.excluir(id);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping
    public ResponseEntity<MensagemRetorno> editar(@RequestBody DepartamentoArea departamentoArea) {

        var msgRetorno = this.departamentoServico.editar(departamentoArea);

        return ResponseEntity.ok().body(msgRetorno);
    }
}
