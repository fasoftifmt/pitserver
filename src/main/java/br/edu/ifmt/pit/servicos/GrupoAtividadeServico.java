package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.GrupoAtividade;
import br.edu.ifmt.pit.exceptions.DataIntegrityException;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.GrupoAtividadeRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GrupoAtividadeServico {

    @Autowired
    private GrupoAtividadeRepositorio grupoAtividadeRepositorio;

    public GrupoAtividade buscar(Integer id) {
        Optional<GrupoAtividade> obj = grupoAtividadeRepositorio.findById(id);
        return obj.orElse(null);
    }

    private MensagemRetorno isGrupoAtividadeValida(GrupoAtividade grupoAtividade){
        if(grupoAtividade == null) return new MensagemRetorno(false, "Dados de Grupo Atividade inválidos");

        var mensagemRetorno = new MensagemRetorno();
        mensagemRetorno.setSucesso(true);
        if(grupoAtividade.getDescricao() == null || grupoAtividade.getDescricao().isEmpty()) {
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Descrição inválida");
        }

        if(grupoAtividade.getOrdem() == null || grupoAtividade.getOrdem() < 0) {
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Ordem inválida");
        }

        return mensagemRetorno;
    }

/*    public void salvar(List<GrupoAtividade> atividadeList) {
        this.grupoAtividadeRepositorio.saveAll(atividadeList);
    }*/

    public MensagemRetorno salvar(GrupoAtividade grupoAtividade){
        MensagemRetorno grupoAtividadeValida = this.isGrupoAtividadeValida(grupoAtividade);

        if(!grupoAtividadeValida.isSucesso()) return grupoAtividadeValida;

        try{

            this.grupoAtividadeRepositorio.save(grupoAtividade);
            grupoAtividadeValida.setSucesso(true);
            grupoAtividadeValida.addMsg("Sucesso ao salvar o grupo atividade");
            return grupoAtividadeValida;
        } catch (Exception e) {
            e.printStackTrace();
            grupoAtividadeValida.setSucesso(false);
            grupoAtividadeValida.addMsg(e.getMessage());
            return grupoAtividadeValida;
        }

    }

    public MensagemRetorno excluir(Integer id) {
        try {
            this.grupoAtividadeRepositorio.deleteById(id);
            return new MensagemRetorno(true, "Grupo Atividade excluída com sucesso");
        } catch (DataIntegrityException ex) {
            return new MensagemRetorno(false, "Não é possível excluir pois o Grupo Atividade está sendo usado");
        }
    }

    public Page<GrupoAtividade> buscaPaginada(String descricao, Integer page, Integer linesPerPage, String orderBy, String direction){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return grupoAtividadeRepositorio.search(descricao, pageRequest);
    }

    public List<GrupoAtividade> listarTodos(){
        return this.grupoAtividadeRepositorio.findAll();
    }

   public List<GrupoAtividade> buscarTodos() {
        List<GrupoAtividade> grupoAtividadeList = this.grupoAtividadeRepositorio.getAllByOrdemIsNotNullOrderByOrdemAsc();

        return grupoAtividadeList;
    }

}
