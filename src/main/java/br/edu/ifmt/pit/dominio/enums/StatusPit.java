package br.edu.ifmt.pit.dominio.enums;

public enum StatusPit {

    PLANEJADO(0, "Planejado"),
    ENVIADO(1, "Enviado"),
    APROVADO(2, "Aprovado"),
    PUBLICADO(3, "Publicado"),
    ARQUIVADO(4, "Arquivado");

    private int cod;
    private String descricao;

    StatusPit(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public int getCod() {
        return cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public static StatusPit toEnum(Integer cod){
        if (cod == null) {
            return null;
        }

        for (StatusPit x: StatusPit.values()) {
            if (cod.equals(x.getCod())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

    public static StatusPit toEnum(String  descricao){
        if (descricao == null) {
            return null;
        }

        for (StatusPit x: StatusPit.values()) {
            if (descricao.equalsIgnoreCase(x.getDescricao())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Descricao inválido: " + descricao);
    }
}
