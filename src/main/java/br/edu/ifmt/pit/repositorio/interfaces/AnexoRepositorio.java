package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.AnexoPlanoDeTrabalho;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AnexoRepositorio extends JpaRepository<AnexoPlanoDeTrabalho, Long> {

}
