package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.Curso;
import br.edu.ifmt.pit.dominio.Disciplina;
import br.edu.ifmt.pit.dominio.dtos.DisciplinaDTO;
import br.edu.ifmt.pit.exceptions.DataIntegrityException;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.CursoRepositorio;
import br.edu.ifmt.pit.repositorio.interfaces.DisciplinaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DisciplinaServico {

    @Autowired
    private DisciplinaRepositorio disciplinaRepositorio;

    @Autowired
    private CursoRepositorio cursoRepositorio;

    public Disciplina buscar(Long id) {
        Optional<Disciplina> obj = disciplinaRepositorio.findById(id);
        return obj.orElse(null);
    }

    private MensagemRetorno isDisciplinaValida(DisciplinaDTO disciplina) {
        if (disciplina == null) return new MensagemRetorno(false, "Dados da Disciplina inválidos");

        var mensagemRetorno = new MensagemRetorno();
        mensagemRetorno.setSucesso(true);

        if (disciplina.getDescricao() == null || disciplina.getDescricao().isEmpty()) {
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Descrição inválida");
        }

        if (disciplina.getCargaHoraria() == null || disciplina.getCargaHoraria() < 0
                || disciplina.getCargaHoraria() > 300) {
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Carga Horária inválida");
        }

        // TESTAR NumAulaSemanal ATRIUBUINDO VAZIO NELA PARA VERIFICAR SE ESTA OK
        if(disciplina.getNumAulaSemanal() == ' ' || disciplina.getNumAulaSemanal() < 1
                || disciplina.getNumAulaSemanal() > 20) {
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Número de Aulas Semanais inválido");
        }

        if(disciplina.getPeriodo() == ' ' || disciplina.getPeriodo() < 1
                || disciplina.getPeriodo() > 10) {
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Período da Disciplina inválido");
        }

        if (disciplina.getCursoId() == null){
            mensagemRetorno.setSucesso(false);
            mensagemRetorno.addMsg("Curso não selecionado");
        }

        return mensagemRetorno;
    }

    public MensagemRetorno salvar(DisciplinaDTO disciplina) {
        MensagemRetorno disciplinaValida = this.isDisciplinaValida(disciplina);

        if (!disciplinaValida.isSucesso()) return disciplinaValida;

        try {
            this.disciplinaRepositorio.save(this.fromDTO(disciplina));
            disciplinaValida.setSucesso(true);
            disciplinaValida.addMsg("Sucesso ao salvar a disciplina");
            return disciplinaValida;
        } catch (Exception e) {
            e.printStackTrace();
            disciplinaValida.setSucesso(false);
            disciplinaValida.addMsg(e.getMessage());
            return disciplinaValida;
        }
    }

    public Disciplina fromDTO(DisciplinaDTO disciplinaDTO){
        var disciplina = new Disciplina();
        disciplina.setId(disciplinaDTO.getId());
        disciplina.setCargaHoraria(disciplinaDTO.getCargaHoraria());
        disciplina.setDescricao(disciplinaDTO.getDescricao());
        disciplina.setNumAulaSemanal(disciplinaDTO.getNumAulaSemanal());
        disciplina.setPeriodo(disciplinaDTO.getPeriodo());
        Optional<Curso> curso = this.cursoRepositorio.findById(disciplinaDTO.getCursoId());
        curso.ifPresent(disciplina::setCurso);

        return disciplina;
    }

    public MensagemRetorno excluir(Long id) {
        try {
            this.disciplinaRepositorio.deleteById(id);
            return new MensagemRetorno(true, "Disciplina excluída com sucesso");
        } catch (DataIntegrityException ex) {
            return new MensagemRetorno(false, "Não é possível excluir pois a disciplina está sendo usado");
        }
    }

    public Page<Disciplina> buscaPaginada(String descricao, Integer page, Integer linesPerPage, String orderBy, String direction){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return disciplinaRepositorio.search(descricao, pageRequest);
    }

    public List<Disciplina> listarTodos(){
        return this.disciplinaRepositorio.findAll();
    }


}