package br.edu.ifmt.pit.dominio.dtos;

import java.io.Serializable;
import java.util.Objects;

public class AvaliadorDTO implements Serializable {

    private static final long serialVersionID = 1L;

    private Long id;
    private String descricao;
    private Long servidorID;

    public AvaliadorDTO(){}
    public AvaliadorDTO(Long id, String descricao, Long servidorID){
        this.descricao = descricao;
        this.servidorID = servidorID;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getServidorID() {
        return servidorID;
    }

    public void setServidorID(Long servidorID) {
        this.servidorID = servidorID;
    }

    @Override public int hashCode(){ return Objects.hash(id);}

}
