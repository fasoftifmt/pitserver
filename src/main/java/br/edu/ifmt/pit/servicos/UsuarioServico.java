package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.Usuario;
import br.edu.ifmt.pit.dominio.dtos.UsuarioDTO;
import br.edu.ifmt.pit.dominio.enums.PerfilUsuario;
import br.edu.ifmt.pit.exceptions.AuthorizationException;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.UsuarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UsuarioServico implements UserDetailsService {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    public Usuario buscar(Long id) {
        Optional<Usuario> obj = usuarioRepositorio.findById(id);
        return obj.orElse(null);
    }
    
    
    private boolean isUsuarioValido(Usuario usuario){
        return  usuario.getLogin() != null &&
                !usuario.getLogin().isEmpty() &&
                usuario.getSenha() != null &&
                !usuario.getSenha().isEmpty();
    }

    public MensagemRetorno excluir(Long id){
      try {
          this.usuarioRepositorio.deleteById(id);
          return new MensagemRetorno(true, "Sucesso ao excluir");
      }catch(Exception ex) {

          return new MensagemRetorno(true, ex.getMessage());
      }
    }

    public Usuario getUsuarioByLogin(String login) {
        Usuario usuario = usuarioRepositorio.findByLogin(login);
        System.out.println("================================================");
        System.out.println("================================================");
        System.out.println("================================================");
        System.out.println("================================================");
        System.out.println(usuario.toString());
        System.out.println("================================================");
        System.out.println("================================================");
        System.out.println("================================================");
        System.out.println("================================================");
        return usuario;
    }

    public MensagemRetorno inserir(Usuario usuario){
        if(!isUsuarioValido(usuario)) return new MensagemRetorno(false, "Dados do usuario invalido");
        try{
            this.usuarioRepositorio.save(usuario);
            return new MensagemRetorno(true, "Sucesso ao inserir registro");
        }catch(Exception e){
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }
    }
    public MensagemRetorno editar(Usuario usuario){
        if(!isUsuarioValido(usuario)) return new MensagemRetorno(false, "Dados do usuario sao invalidos");

        try{
            this.usuarioRepositorio.save((usuario));
            return new MensagemRetorno(true, "Sucesso ao editar registro");
        }catch (Exception e){
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }
    }

    public List<Usuario> listarTodos() {
        return this.usuarioRepositorio.findAll();
    }


    public Usuario fromDTO(UsuarioDTO u){
        System.out.println("USUARIO_DTO: " + u.toString());
        Usuario usuario = new Usuario(null, u.getLogin(), u.getSenha());
        Set<PerfilUsuario> set = u.perfilUsuariofromDTO();
        return usuario;
    }

    @Override
    public Usuario loadUserByUsername(String login) throws UsernameNotFoundException {
        var usuario = usuarioRepositorio.findByLogin(login);
//        var usuario = usuarioRepositorio.findByLogin(login);
        if (usuario == null) {
            throw new UsernameNotFoundException(login);
        }
        return usuario;
    }

    public static Usuario authenticated() {
        try {
            return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean isUsuarioAutorizado(PerfilUsuario perfilUsuario) {
        Usuario user = authenticated();
        if (user == null) return false;
        return user.hasRole(perfilUsuario);
    }

    public void isIdUsuarioIgualAoDoToken(Long idUsuario) throws AuthorizationException {
        Usuario user = authenticated();

        if (user == null || user.hasRole(PerfilUsuario.ADMIN) || user.hasRole(PerfilUsuario.GESTOR)) {
            return;
        }
        if (!idUsuario.equals(user.getIdUsuario())) {
            throw new AuthorizationException("Acesso negado");
        }
    }

//    public Page<Usuario> buscaPaginada(String usuarioDeconded, Integer page, Integer linesPerPage, String orderBy, String direction) {
//        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
//        return usuarioRepositorio.search(usuarioDeconded, pageRequest);
//    }
}
