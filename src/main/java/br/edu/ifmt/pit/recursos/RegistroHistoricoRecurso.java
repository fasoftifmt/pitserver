package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.RegistroHistorico;
import br.edu.ifmt.pit.dominio.dtos.RegistroHistoricoDTO;
import br.edu.ifmt.pit.servicos.RegistroHistoricoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/registro")
public class RegistroHistoricoRecurso {

    @Autowired
    private RegistroHistoricoServico registroServico;

    @GetMapping(value = "/{id}")
    public ResponseEntity<RegistroHistorico> buscar(@PathVariable Long id) {
        var registro = registroServico.buscar(id);
        return ResponseEntity.ok().body(registro);
    }

    @PreAuthorize("hasAnyRole('SERVIDOR')")
    @PostMapping(value = "/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody RegistroHistoricoDTO registro){
        var mensagemRetorno = this.registroServico.inserir(registro);

        if (mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        }else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);

        }
    }
}
