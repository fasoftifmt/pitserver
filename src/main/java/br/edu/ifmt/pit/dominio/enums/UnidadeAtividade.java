package br.edu.ifmt.pit.dominio.enums;

public enum UnidadeAtividade {

    HORAS(1, "Horas"),
    AULAS(2, "Aulas");

    private final Integer cod;
    private final String descricao;

    UnidadeAtividade(Integer cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public Integer getCod() {
        return cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public UnidadeAtividade toEnum(Integer cod) {
        if (cod == null) throw new IllegalArgumentException("O código não pode ser nulo");

        for (UnidadeAtividade unidadeAtividade : UnidadeAtividade.values()) {
            if (unidadeAtividade.cod.equals(cod)) return unidadeAtividade;
        }
        throw new IllegalArgumentException(
                String.format("O código não corresponde a nenhum valor de %s, cod: %s",
                        UnidadeAtividade.class.getName(), cod)
        );
    }
}
