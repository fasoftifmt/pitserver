package br.edu.ifmt.pit.dominio.enums;

public enum SituacaoServidor {

    ATIVO(1, "Ativo"),
    AFASTADO(2, "Afastado"),
    INATIVO(3, "Inativo");

    private int cod;
    private String descricao;

    SituacaoServidor(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public int getCod() {
        return cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public static SituacaoServidor toEnum(Integer cod){
        if (cod == null) {
            return null;
        }

        for (SituacaoServidor x: SituacaoServidor.values()) {
            if (cod.equals(x.getCod())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }
}
