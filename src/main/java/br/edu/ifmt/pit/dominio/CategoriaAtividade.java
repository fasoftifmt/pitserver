package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class CategoriaAtividade implements Serializable {

    private static final long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer ordem;
    private String descricao;

    @JsonIgnoreProperties("categoria")
    @OneToMany(mappedBy = "categoria")
    private List<Atividade> atividadeModelList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "grupo_id")
    private GrupoAtividade grupoAtividade;

    public CategoriaAtividade() {
    }

    public CategoriaAtividade(Integer id, String descricao, GrupoAtividade grupoAtividade, Integer ordem) {
        this.descricao = descricao;
        this.id = id;
        this.grupoAtividade = grupoAtividade;
        this.ordem = ordem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Atividade> getAtividadeModelList() {
        return atividadeModelList;
    }

    public void setAtividadeModelList(List<Atividade> atividadeModelList) {
        this.atividadeModelList = atividadeModelList;
    }

    public GrupoAtividade getGrupoAtividade() {
        return grupoAtividade;
    }

    public void setGrupoAtividade(GrupoAtividade grupoAtividade) {
        this.grupoAtividade = grupoAtividade;
    }

    public Integer getOrdem() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoriaAtividade that = (CategoriaAtividade) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void addAtividade(Atividade sub02) {
        this.atividadeModelList.add(sub02);
    }
}
