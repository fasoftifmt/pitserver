package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.Atividade;
import br.edu.ifmt.pit.dominio.dtos.SubAtividadeDTO;
import br.edu.ifmt.pit.repositorio.interfaces.AtividadeRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class SubAtividadeServico {

    @Autowired
    private AtividadeRepositorio atividadeRepositorio;

    public Atividade buscar(Long id){
        Optional<Atividade> obj = atividadeRepositorio.findById(id);
        return obj.orElse(null);
    }

    public List<SubAtividadeDTO> buscarTodos() {
        return this.toDTO(this.atividadeRepositorio.findAll());
    }

    private List<SubAtividadeDTO> toDTO(List<Atividade> atividades) {
       var atividadeDTOList = new ArrayList<SubAtividadeDTO>();
        atividades.forEach( atividade -> {

            SubAtividadeDTO atividadeDTO = new SubAtividadeDTO();

            atividadeDTO.setCategoria(atividade.getCategoria());
            atividadeDTO.setDescricao(atividade.getDescricao());
            atividadeDTO.setDicaPreenchimento(atividade.getDicaPreenchimento());
            atividadeDTO.setFator(atividade.getFator());
            atividadeDTO.setId(atividade.getId());
            atividadeDTO.setUnidadeAtividade(atividade.getUnidadeAtividade());
            atividadeDTO.setAtividadeList(atividade.getSubAtividadeList());
            atividadeDTOList.add(atividadeDTO);

        });

        return atividadeDTOList;
    }
}
