package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.JustificativaEnsino;
import br.edu.ifmt.pit.repositorio.interfaces.JustificativaEnsinoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class JustificativaEnsinoServico {

    @Autowired
    private JustificativaEnsinoRepositorio justificativaEnsinoRepositorio;

    public JustificativaEnsino buscar(Integer id){
        Optional<JustificativaEnsino> obj = justificativaEnsinoRepositorio.findById(id);
        return obj.orElse(null);
    }

    @Transactional
    public void salvar(List<JustificativaEnsino> atividadePitList) {
        this.justificativaEnsinoRepositorio.saveAll(atividadePitList);
    }
}
