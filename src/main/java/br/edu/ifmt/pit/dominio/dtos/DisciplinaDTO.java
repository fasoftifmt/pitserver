package br.edu.ifmt.pit.dominio.dtos;


public class DisciplinaDTO {

    private static final long serialVersionID = 1L;

    private Long id;
    private String descricao;
    private Double cargaHoraria;
    private int numAulaSemanal;
    private int periodo;

    private Long cursoId;

    public DisciplinaDTO(Long id, String descricao, Double cargaHoraria, int numAulaSemanal, int periodo, Long cursoId) {
        this.id = id;
        this.descricao = descricao;
        this.cargaHoraria = cargaHoraria;
        this.numAulaSemanal = numAulaSemanal;
        this.periodo = periodo;
        this.cursoId = cursoId;
    }


    public DisciplinaDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(Double cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public int getNumAulaSemanal() { return numAulaSemanal; }

    public void setNumAulaSemanal(int numAulaSemanal) { this.numAulaSemanal = numAulaSemanal; }

    public int getPeriodo() { return periodo; }

    public void setPeriodo(int periodo) {this.periodo = periodo; }

    public Long getCursoId() {
        return cursoId;
    }

    public void setCursoId(Long cursoId) {
        this.cursoId = cursoId;
    }
}
