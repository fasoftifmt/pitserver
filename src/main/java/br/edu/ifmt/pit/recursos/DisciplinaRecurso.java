package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.Disciplina;
import br.edu.ifmt.pit.dominio.dtos.DisciplinaDTO;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.DisciplinaServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/disciplinas")
public class DisciplinaRecurso {

    @Autowired
    private DisciplinaServico disciplinaServico;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Disciplina> buscar(@PathVariable Long id){
        var disciplina = disciplinaServico.buscar(id);
        return ResponseEntity.ok().body(disciplina);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody DisciplinaDTO disciplina) {

        var mensagemRetorno = this.disciplinaServico.salvar(disciplina);

        if (mensagemRetorno.isSucesso()) {
            return ResponseEntity.ok().body(mensagemRetorno);
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value="/pagina")
    public ResponseEntity<Page<Disciplina>> findPage(
            @RequestParam(value = "descricao", defaultValue= "") String descricao,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "descricao") String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction){

        String descricaoDeconded = URL.decodeParam(descricao);

        Page<Disciplina> lista = disciplinaServico.buscaPaginada(descricaoDeconded, page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(lista);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MensagemRetorno> excluir(@PathVariable Long id) {

        var msgRetorno = this.disciplinaServico.excluir(id);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping
    public ResponseEntity<MensagemRetorno> editar(@RequestBody DisciplinaDTO disciplina){

        var msgRetorno = this.disciplinaServico.salvar(disciplina);

        return ResponseEntity.ok().body(msgRetorno);
    }
}