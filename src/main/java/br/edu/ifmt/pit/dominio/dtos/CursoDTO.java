package br.edu.ifmt.pit.dominio.dtos;

import br.edu.ifmt.pit.dominio.DepartamentoArea;
import br.edu.ifmt.pit.dominio.Disciplina;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CursoDTO implements Serializable {

    private static final long serialVersionID = 1L;


    private Long id;
    private String descricao;
    private Long departamentoID;


    private List<Disciplina> disciplinas = new ArrayList<>();

    public CursoDTO() {
    }

    public CursoDTO(Long id, String descricao, Long departamentoID) {
        this.descricao = descricao;
        this.departamentoID = departamentoID;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getDepartamentoID() {
        return departamentoID;
    }

    public void setDepartamentoID(Long departamentoID) {
        this.departamentoID = departamentoID;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }


    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
