package br.edu.ifmt.pit.dominio.dtos;

import br.edu.ifmt.pit.dominio.enums.RegimeTrabalho;
import br.edu.ifmt.pit.dominio.enums.TipoDeVinculo;

public class ServidorDTO {
    private Long id;
    private Long departamentoId;
    private String nome;
    private String email;
    private String matriculaSiape;
    private String fone;
    private Integer regimeInteger;
    private Integer situacaoInteger;
    private Integer vinculoInteger;

    public ServidorDTO(Long id, Long departamentoId, Integer regimeInteger,
                       Integer situacaoInteger, Integer vinculoInteger,
                       String nome, String email, String matriculaSiape, String fone) {
        this.id = id;
        this.departamentoId = departamentoId;
        this.regimeInteger = regimeInteger;
        this.situacaoInteger = situacaoInteger;
        this.vinculoInteger = vinculoInteger;
        this.nome = nome;
        this.email = email;
        this.matriculaSiape = matriculaSiape;
        this.fone = fone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMatriculaSiape() {
        return matriculaSiape;
    }

    public void setMatriculaSiape(String matriculaSiape) {
        this.matriculaSiape = matriculaSiape;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }
}
