# Para configurar o ambiente realize os passos a seguir: #

- Instale o GIT 
[Download GIT](https://git-scm.com/download/win)

- Faça o clone do projeto
    * Acesse o repositório do projeto e clique em clone.
     ![passo01 java.PNG](https://bitbucket.org/repo/Knr4oqM/images/4238902821-passo01%20java.PNG)
    * Certifique-se que esteja marcado HTTPS e Copie o comando.
     ![passo02 java.PNG](https://bitbucket.org/repo/Knr4oqM/images/2501847340-passo02%20java.PNG)
- Abra o console do GIT BASH e navegue até a pasta onde deseja que fique o projeto
    * Cole o comando e execute, será iniciado o clone do projeto aguarde...
- Realize o download do JDK 11
[Download java 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
      * Instale o JDK 11
      * Crie uma variável de ambiente para o java 
      ![passo03PNG.PNG](https://bitbucket.org/repo/Knr4oqM/images/2767190266-passo03PNG.PNG)
      * Defina o JAVA_HOME para o JDK 11
      ![passo04.PNG](https://bitbucket.org/repo/Knr4oqM/images/542643413-passo04.PNG)

- Realize o download do MySql 8 
[Download MySql](https://dev.mysql.com/downloads/windows/installer/8.0.html)

- Instale o Servidor e o WorkBench

- Baixe uma IDE com suporte para Java 11 e Spring (STS ou IntelliJ IDEA)
[Download STS 3.9.9](https://spring.io/tools3/sts/all)
[Download Intellij IDEA 3.9.9](https://www.jetbrains.com/idea/)

- Abra o projeto e execute a aplicação