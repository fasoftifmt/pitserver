package br.edu.ifmt.pit.dominio.dtos;

import br.edu.ifmt.pit.dominio.Atividade;
import br.edu.ifmt.pit.dominio.CategoriaAtividade;
import br.edu.ifmt.pit.dominio.enums.UnidadeAtividade;

import java.util.ArrayList;
import java.util.List;

public class SubAtividadeDTO {

    private Long id;
    private Double fator;
    private String descricao;
    private String dicaPreenchimento;
    private UnidadeAtividade unidadeAtividade;
    private CategoriaAtividade categoria;
    private List<Atividade> atividadeList = new ArrayList<>();

    public SubAtividadeDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFator() {
        return fator;
    }

    public void setFator(Double fator) {
        this.fator = fator;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDicaPreenchimento() {
        return dicaPreenchimento;
    }

    public void setDicaPreenchimento(String dicaPreenchimento) {
        this.dicaPreenchimento = dicaPreenchimento;
    }

    public UnidadeAtividade getUnidadeAtividade() {
        return unidadeAtividade;
    }

    public void setUnidadeAtividade(UnidadeAtividade unidadeAtividade) {
        this.unidadeAtividade = unidadeAtividade;
    }

    public CategoriaAtividade getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaAtividade categoria) {
        this.categoria = categoria;
    }

    public List<Atividade> getAtividadeList() {
        return atividadeList;
    }

    public void setAtividadeList(List<Atividade> atividadeList) {
        this.atividadeList = atividadeList;
    }
}
