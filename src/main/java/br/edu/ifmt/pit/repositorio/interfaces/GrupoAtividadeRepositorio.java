package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.GrupoAtividade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface GrupoAtividadeRepositorio extends JpaRepository<GrupoAtividade, Integer> {

    public List<GrupoAtividade> getAllByOrdemIsNotNullOrderByOrdemAsc();

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM GrupoAtividade obj WHERE obj.descricao LIKE %:descricao%")
    Page<GrupoAtividade> search(@Param("descricao") String descricao, Pageable pageRequest);
}
