package br.edu.ifmt.pit.dominio.dtos;

import br.edu.ifmt.pit.dominio.enums.StatusPit;

import java.util.Date;
import java.util.Objects;

public class PlanoDeTrabalhoResumidoDTO {

    private Long id;
    private Date dataEnvio;
    private StatusPit statusPtd;

    public PlanoDeTrabalhoResumidoDTO(Long id, Date dataEnvio, StatusPit statusPtd) {
        this.id = id;
        this.dataEnvio = dataEnvio;
        this.statusPtd = statusPtd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public StatusPit getStatusPtd() {
        return statusPtd;
    }

    public void setStatusPtd(StatusPit statusPtd) {
        this.statusPtd = statusPtd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlanoDeTrabalhoResumidoDTO)) return false;

        PlanoDeTrabalhoResumidoDTO that = (PlanoDeTrabalhoResumidoDTO) o;

        if (!Objects.equals(id, that.id)) return false;
        if (!Objects.equals(dataEnvio, that.dataEnvio)) return false;
        return statusPtd == that.statusPtd;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dataEnvio != null ? dataEnvio.hashCode() : 0);
        result = 31 * result + (statusPtd != null ? statusPtd.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PlanoDeTrabalhoResumidoDTO{" +
                "id=" + id +
                ", dataEnvio=" + dataEnvio +
                ", statusPtd=" + statusPtd +
                '}';
    }
}
