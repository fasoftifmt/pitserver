package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.GrupoAtividade;
import br.edu.ifmt.pit.dominio.dtos.SubAtividadeDTO;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.GrupoAtividadeServico;
import br.edu.ifmt.pit.servicos.SubAtividadeServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/grupoAtividades")
public class GrupoAtividadeRecurso {

    @Autowired
    private GrupoAtividadeServico grupoAtividadeServico;
    @Autowired
    private SubAtividadeServico subAtividadeServico; //DUVIDA NESSE ATRIBUTO


    @GetMapping(value = "/{id}")
    public ResponseEntity<GrupoAtividade> buscar(@PathVariable Integer id){
        var categoriaAtividade = grupoAtividadeServico.buscar(id);
        return ResponseEntity.ok().body(categoriaAtividade);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value="/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody GrupoAtividade grupoAtividade){
        var mensagemRetorno = this.grupoAtividadeServico.salvar(grupoAtividade);

        if(mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value="/pagina")
    public ResponseEntity<Page<GrupoAtividade>> findPage(
            @RequestParam(value = "descricao", defaultValue= "") String descricao,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "descricao") String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction){

        String descricaoDeconded = URL.decodeParam(descricao);

        Page<GrupoAtividade> lista = grupoAtividadeServico.buscaPaginada(descricaoDeconded, page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(lista);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MensagemRetorno> excluir(@PathVariable Integer id) {

        var msgRetorno = this.grupoAtividadeServico.excluir(id);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping
    public ResponseEntity<MensagemRetorno> editar(@RequestBody GrupoAtividade grupoAtividade){

        var msgRetorno = this.grupoAtividadeServico.salvar(grupoAtividade);

        return ResponseEntity.ok().body(msgRetorno);
    }


    @GetMapping(value = "/buscarTodos")
    public ResponseEntity<List<GrupoAtividade>> buscarTodos(){
        List<GrupoAtividade> categorias = grupoAtividadeServico.buscarTodos();
        return ResponseEntity.ok().body(categorias);
    }

    @GetMapping(value = "/buscarTodasSubAtividades")
    public ResponseEntity<List<SubAtividadeDTO>> buscarTodosSubAtividades(){
        List<SubAtividadeDTO> subAtividadeDTOList = this.subAtividadeServico.buscarTodos();
        return ResponseEntity.ok().body(subAtividadeDTOList);
    }


}
