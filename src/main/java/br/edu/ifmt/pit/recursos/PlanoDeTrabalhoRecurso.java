package br.edu.ifmt.pit.recursos;

import br.edu.ifmt.pit.dominio.PlanoIndividualDeTrabalho;
import br.edu.ifmt.pit.dominio.dtos.PlanoDeTrabalhoResumidoDTO;
import br.edu.ifmt.pit.dominio.dtos.PlanoIndividualDeTrabalhoDTO;
import br.edu.ifmt.pit.servicos.PlanoDeTrabalhoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/pit")
public class PlanoDeTrabalhoRecurso {

    @Autowired
    private PlanoDeTrabalhoServico planoDeTrabalhoServico;

    @GetMapping(value = "/{id}")
    public ResponseEntity<PlanoIndividualDeTrabalho> buscar(@PathVariable Long id) {
        var planoDeTrabalhoDocente = planoDeTrabalhoServico.buscar(id);
        return ResponseEntity.ok().body(planoDeTrabalhoDocente);
    }

    @Transactional
    @PostMapping(value = "/novo")
    public ResponseEntity<Void> insert(@RequestBody PlanoIndividualDeTrabalhoDTO obj) {
        var pit = planoDeTrabalhoServico.insert(obj);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(pit.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping(value = "/buscarPits/{idServidor}")
    public ResponseEntity<List<PlanoDeTrabalhoResumidoDTO>> buscarPlanosDeTrabalho(@PathVariable Long idServidor) {
        List<PlanoDeTrabalhoResumidoDTO> pitList = this.planoDeTrabalhoServico.buscarPlanosDeTrabalhoPorServidor(idServidor);

        return ResponseEntity.ok().body(pitList);
    }

    @PreAuthorize("hasAnyRole('GESTOR')")
    @GetMapping(value = "/buscarParaAvaliar/{idServidor}")
    public ResponseEntity<List<PlanoDeTrabalhoResumidoDTO>> buscarPlanosDeTrabalhoParaAvaliar(@PathVariable Long idServidor){
        List<PlanoDeTrabalhoResumidoDTO> pitList = this.planoDeTrabalhoServico.buscarPlanosDeTrabalhoParaAvaliar(idServidor);

        return ResponseEntity.ok().body(pitList);
    }
}
