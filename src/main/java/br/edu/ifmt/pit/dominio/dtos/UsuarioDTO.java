package br.edu.ifmt.pit.dominio.dtos;

import br.edu.ifmt.pit.dominio.enums.PerfilUsuario;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UsuarioDTO {

    private Long idUsuario;
    private String login;
    private String senha;
    private List<Integer> perfilUsuario;


    public UsuarioDTO(Long idUsuario, String login, String senha, List<Integer> perfilUsuario) {
        this.idUsuario = idUsuario;
        this.login = login;
        this.senha = senha;
        this.perfilUsuario = perfilUsuario;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Integer> getPerfilUsuario() {
        return perfilUsuario;
    }

    public void setPerfilUsuario(List<Integer> perfilUsuario) {
        this.perfilUsuario = perfilUsuario;
    }


    public HashSet<PerfilUsuario> perfilUsuariofromDTO(){
        HashSet<PerfilUsuario> list = new HashSet<PerfilUsuario>();
        for(Integer i : this.perfilUsuario) {
            list.add(PerfilUsuario.toEnum(i));
        }
        return list;
    }

    @Override
    public String toString() {
        return "UsuarioDTO{" +
                "idUsuario=" + idUsuario +
                ", login='" + login + '\'' +
                ", senha='" + senha + '\'' +
                ", perfilUsuario=" + perfilUsuario +
                '}';
    }
}
