package br.edu.ifmt.pit;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PtdIfmtApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PtdIfmtApplication.class, args);
    }

    @Override
    public void run(String... args) {

    }
}
