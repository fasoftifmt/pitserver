package br.edu.ifmt.pit.dominio.dtos;

public class AnexoPlanoDeTrabalhoDTO {

    private Long id;
    private String nome;
    private String descricao;
    private String tipo;
    private String docBase64;

    public AnexoPlanoDeTrabalhoDTO() {
    }

    public AnexoPlanoDeTrabalhoDTO(String nome, String descricao, String tipo, String docBase64) {
        this.nome = nome;
        this.descricao = descricao;
        this.tipo = tipo;
        this.docBase64 = docBase64;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDocBase64() {
        return docBase64;
    }

    public void setDocBase64(String docBase64) {
        this.docBase64 = docBase64;
    }

}
