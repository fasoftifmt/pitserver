package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class GrupoAtividade implements Serializable {

    private static final long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String descricao;
    private Integer ordem;

    @JsonIgnoreProperties("grupoAtividade")
    @OneToMany(mappedBy = "grupoAtividade")
    private List<CategoriaAtividade> categoriaAtividadeList = new ArrayList<>();

    public GrupoAtividade() {
    }

    public GrupoAtividade(Integer id, String descricao, Integer ordem) {
        this.id = id;
        this.descricao = descricao;
        this.ordem = ordem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getOrdem() {
        return ordem;
    }

    public void setOrdem(Integer ordem) {
        this.ordem = ordem;
    }

    public List<CategoriaAtividade> getCategoriaAtividadeList() {
        return categoriaAtividadeList;
    }

    public void setCategoriaAtividadeList(List<CategoriaAtividade> categoriaAtividadeList) {
        this.categoriaAtividadeList = categoriaAtividadeList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GrupoAtividade)) return false;
        GrupoAtividade that = (GrupoAtividade) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
