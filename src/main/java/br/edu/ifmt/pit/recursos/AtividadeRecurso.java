package br.edu.ifmt.pit.recursos;
import br.edu.ifmt.pit.dominio.Atividade;
import br.edu.ifmt.pit.dominio.dtos.AtividadeDTO;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.AtividadeServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/atividades")
public class AtividadeRecurso {

    @Autowired
    private AtividadeServico atividadeServico;

    @GetMapping(value = "/buscarTodos")
    public ResponseEntity<List<Atividade>> buscarTodos(){
        List<Atividade> categorias = atividadeServico.buscarTodos();
        return ok().body(categorias);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody AtividadeDTO atividade){
        var mensagemRetorno = this.atividadeServico.inserir(atividade);

        if(mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        }else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value = "/pagina")
    public ResponseEntity<Page<Atividade>> findPage(
            @RequestParam(value = "descricao", defaultValue = "") String descricao,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "descricao") String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction) {

        String descricaoDecoded = URL.decodeParam(descricao);

        Page<Atividade> lista = atividadeServico.buscaPaginada(descricaoDecoded, page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(lista);
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping
    public ResponseEntity<MensagemRetorno> editar(@RequestBody AtividadeDTO atividade) {

        var msgRetorno = this.atividadeServico.editar(atividade);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<MensagemRetorno> excluir(@PathVariable Long id) {

        var msgRetorno = this.atividadeServico.excluir(id);

        return ResponseEntity.ok().body(msgRetorno);
    }

   //@PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/vincular")
    public ResponseEntity<MensagemRetorno> vincularAtividades(@RequestBody List<Integer> ids ){

        var mensagemRetorno = this.atividadeServico.vincularAtividades(ids);

        if(mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        }else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PutMapping(value = "/desvincular")
    public ResponseEntity<MensagemRetorno> desvincularAtividades(@RequestBody List<Integer> ids){

        var mensagemRetorno = this.atividadeServico.desvincularAtividades(ids);

        if(mensagemRetorno.isSucesso()){
            return ResponseEntity.ok().body(mensagemRetorno);
        }else{
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }

    }

}
