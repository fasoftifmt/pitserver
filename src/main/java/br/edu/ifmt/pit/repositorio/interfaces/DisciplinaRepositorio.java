package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.DepartamentoArea;
import br.edu.ifmt.pit.dominio.Disciplina;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface DisciplinaRepositorio extends JpaRepository<Disciplina, Long> {

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM Disciplina obj WHERE obj.descricao LIKE %:descricao%")
    Page<Disciplina> search(@Param("descricao") String descricao, Pageable pageRequest);
}