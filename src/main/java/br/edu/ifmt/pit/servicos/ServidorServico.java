package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.Servidor;
import br.edu.ifmt.pit.dominio.Usuario;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.ServidorRepositorio;


import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;



import java.io.InputStream;
import java.util.*;

@Service
public class ServidorServico {

    @Autowired
    private ServidorRepositorio servidorRepositorio;
    @Autowired
    private UsuarioServico usuarioServico;

    public byte[] relatorioServidor(Servidor servidor) throws Exception {
        List<Servidor> dados = servidorRepositorio.findAll();

        Map<String, Object> paramentos = new HashMap<>();
        paramentos.put("REPORT_LOCALE", new Locale("pt", "BR"));

        InputStream inputStream = this.getClass().getResourceAsStream(
                "/relatorios/RelatorioServidor.jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, paramentos,
                new JRBeanCollectionDataSource(dados));
        return JasperExportManager.exportReportToPdf(jasperPrint);

    }

    public List<Servidor> listarTodos(){
       return this.servidorRepositorio.findAll();
    }
    public Servidor buscar(Long id){
        Optional<Servidor> obj = servidorRepositorio.findById(id);
        return obj.orElse(null);
    }

    public Servidor buscarPorLogin(String login){
        return servidorRepositorio.getServidorByUsuarioLogin(login);
    }


    private boolean isServidorValido(Servidor servidor){
        if( servidor == null ) return false;
        System.out.println(servidor.toString());
        return  servidor.getEmail() != null
                && !servidor.getEmail().isEmpty()
                && servidor.getFone() != null
                && !servidor.getFone().isEmpty()
                && servidor.getNome() != null
                && !servidor.getNome().isEmpty()
                && servidor.getMatriculaSiape() != null
                && !servidor.getMatriculaSiape().isEmpty()
                && servidor.getDepartamentoArea() != null;

    }

    public Page<Servidor> buscaPaginada(String nome, Integer page, Integer linesPerPage, String orderBy, String direction ){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return servidorRepositorio.search(nome, pageRequest);
    }

    public MensagemRetorno inserir(Servidor servidor) {
        if( servidor.getUsuario() != null ) {
            var usuario = usuarioServico.getUsuarioByLogin(servidor.getUsuario().getLogin());
            servidor.setUsuario(usuario);
            System.out.println("========================================");
            System.out.println("Servidor: " + servidor);
            System.out.println("Usuario: " + servidor.getUsuario());
            System.out.println("========================================");

            return new MensagemRetorno(true, "Sucesso ao inserir registro");
        }
        if( !this.isServidorValido(servidor)) {
            return new MensagemRetorno(false, "Dados do servidor inválidos");
        }
        try{
            this.servidorRepositorio.save(servidor);
            return new MensagemRetorno(true, "Sucesso ao inserir registro");
        }catch(Exception e){
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }

    }
    public MensagemRetorno excluir(Long id){
        try{
            this.servidorRepositorio.deleteById(id);
            return new MensagemRetorno(true, "Sucesso ao Excluir");
        }catch(DataIntegrityViolationException ex){
            return new MensagemRetorno(false, "Não é possível excluir pois este registro está sendo usado");
        }
    }

    public MensagemRetorno editar(Servidor servidor){
        if(!this.isServidorValido(servidor)){
            return new MensagemRetorno(false, "Dados do Servidor inválidos");
        }
        try{
            this.servidorRepositorio.save(servidor);
            return new MensagemRetorno(true, "Registro atualizado com sucesso!");
        } catch (Exception ex){
            ex.printStackTrace();
            return new MensagemRetorno(false, ex.getMessage());
        }
    }

}
