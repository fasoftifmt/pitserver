package br.edu.ifmt.pit.servicos;


import br.edu.ifmt.pit.dominio.Avaliador;
import br.edu.ifmt.pit.dominio.Servidor;
import br.edu.ifmt.pit.dominio.dtos.AvaliadorDTO;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.AvaliadorRepositorio;
import br.edu.ifmt.pit.repositorio.interfaces.ServidorRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AvaliadorServico {


    @Autowired
    private AvaliadorRepositorio avaliadorRepositorio;

    public Avaliador buscar(Long id){
        Optional<Avaliador> obj = avaliadorRepositorio.findById(id);
        return obj.orElse(null);
    }

    public List<Avaliador> buscarTodos(){return avaliadorRepositorio.findAll();}

    private boolean isAvaliadorValido(Avaliador avaliador){
        if(avaliador == null) return (false);
        return avaliador.getDescricao() != null
                && !avaliador.getDescricao().isEmpty();
    }

    public Page<Avaliador> buscaPaginada(String descricao, Integer page, Integer linesPerPage
    , String orderBy, String direction){
        PageRequest pageRequest = PageRequest.of(page,linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return avaliadorRepositorio.search(descricao, pageRequest);
    }


    public MensagemRetorno excluir(Long id) {
        try{
            this.avaliadorRepositorio.deleteById(id);
            return new MensagemRetorno(true, "Sucesso ao excluir registro");
        }
        catch( DataIntegrityViolationException ex){
            return new MensagemRetorno(false, "Não foi possível excluir pois esse registro está sendo usado");
        }
    }

    public MensagemRetorno editar(Avaliador avaliador) {
        if( !this.isAvaliadorValido(avaliador))
            return new MensagemRetorno(false, "Dados do avaliador não são válidos");
        try{
            this.avaliadorRepositorio.save(avaliador);
            return new MensagemRetorno(true, "Registro atualizado com sucesso");
        }
        catch( Exception ex){
            ex.printStackTrace();
            return new MensagemRetorno(false, ex.getMessage());
        }

    }

    public MensagemRetorno inserir(Avaliador avaliador) {
        if( !this.isAvaliadorValido(avaliador))
            return new MensagemRetorno(false, "Dados do avaliador não são válidos");
        try{
            this.avaliadorRepositorio.save(avaliador);
            return new MensagemRetorno(true,"Sucesso ao inserir registro");
        }
        catch( Exception e){
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }
    }
//
//    public Avaliador fromDTO(AvaliadorDTO dto){
//        var avaliador = new Avaliador();
//        avaliador.setDescricao(dto.getDescricao());
//
//        Optional<Servidor> servidor = this.servidorRepositorio.findById(dto.getServidorID());
//        servidor.ifPresent(avaliador::setServidor);
//
//        avaliador.setIdAvaliador(dto.getId());
//        return avaliador;
//    }

}
