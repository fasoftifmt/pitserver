package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.DepartamentoArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface DepartamentoRepositorio extends JpaRepository<DepartamentoArea, Long> {

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM DepartamentoArea obj WHERE obj.descricao LIKE %:descricao%")
    Page<DepartamentoArea> search(@Param("descricao") String descricao, Pageable pageRequest);
}
