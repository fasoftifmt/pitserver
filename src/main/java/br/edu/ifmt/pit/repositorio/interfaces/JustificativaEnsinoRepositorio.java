package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.JustificativaEnsino;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JustificativaEnsinoRepositorio extends JpaRepository<JustificativaEnsino, Integer> {}
