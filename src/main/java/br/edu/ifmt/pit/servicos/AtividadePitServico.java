package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.repositorio.interfaces.AtividadePitRepositorio;
import br.edu.ifmt.pit.dominio.AtividadePit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AtividadePitServico {

    @Autowired
    private AtividadePitRepositorio atividadePitRepositorio;

    public AtividadePit buscar(Integer id){
        Optional<AtividadePit> obj = atividadePitRepositorio.findById(id);
        return obj.orElse(null);
    }

    @Transactional
    public void salvar(List<AtividadePit> atividadePitList) {
        this.atividadePitRepositorio.saveAll(atividadePitList);
    }
}
