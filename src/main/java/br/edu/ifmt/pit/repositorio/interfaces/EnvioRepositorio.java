package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.Envio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnvioRepositorio extends JpaRepository<Envio, Long> {

}
