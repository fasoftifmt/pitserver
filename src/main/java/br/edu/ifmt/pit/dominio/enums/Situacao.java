package br.edu.ifmt.pit.dominio.enums;

public enum Situacao {

    APROVADO(1, "Aprovado"),
    REPROVADO(2, "Reprovado");

    private int cod;
    private String descricao;

    Situacao(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public int getCod() {
        return cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public static Situacao toEnum(Integer cod){
        if (cod == null) {
            return null;
        }

        for (Situacao x: Situacao.values()) {
            if (cod.equals(x.getCod())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

    public static Situacao toEnum(String  descricao){
        if (descricao == null) {
            return null;
        }

        for (Situacao x: Situacao.values()) {
            if (descricao.equals(x.getDescricao())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Descricao inválido: " + descricao);
    }
}
