package br.edu.ifmt.pit.config;

import br.edu.ifmt.pit.servicos.DBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("hom")
public class HomConfig {

    @Autowired
    private DBService dbService;

    @Bean
    public boolean instantiateDatabase() {
        this.dbService.instantiateDatabase();
        return true;
    }
}
