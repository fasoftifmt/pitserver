package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Curso implements Serializable {

    private static final long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String descricao;

    //@JsonIgnore
    @ManyToOne
    @JsonIgnoreProperties("cursos")
    @JoinColumn(name = "departamento_id")
    private DepartamentoArea departamentoArea;

    @JsonIgnoreProperties("curso")
    @OneToMany(mappedBy = "curso")
    private List<Disciplina> disciplinas = new ArrayList<>();

    public Curso() {
    }

    public Curso(Long id, String descricao, DepartamentoArea departamentoArea) {
        this.descricao = descricao;
        this.departamentoArea = departamentoArea;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public DepartamentoArea getDepartamentoArea() {
        return departamentoArea;
    }

    public void setDepartamentoArea(DepartamentoArea departamentoArea) {
        this.departamentoArea = departamentoArea;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Curso curso = (Curso) o;
        return Objects.equals(id, curso.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
