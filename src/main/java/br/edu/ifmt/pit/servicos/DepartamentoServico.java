package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.DepartamentoArea;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.DepartamentoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartamentoServico {

    @Autowired
    private DepartamentoRepositorio departamentoRepositorio;

    public DepartamentoArea buscar(Long id) {
        Optional<DepartamentoArea> obj = departamentoRepositorio.findById(id);
        return obj.orElse(null);
    }

    public MensagemRetorno inserir(DepartamentoArea departamentoArea) {

        if (!this.isDepartamentoValido(departamentoArea))
            return new MensagemRetorno(false, "Dados do DepartamentoArea inválidos");

        try {
            this.departamentoRepositorio.save(departamentoArea);
            return new MensagemRetorno(true, "Sucesso ao inserir registro");
        } catch (Exception e) {
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }
    }

    private boolean isDepartamentoValido(DepartamentoArea departamentoArea) {
        if (departamentoArea == null) return false;

        return departamentoArea.getDescricao() != null
                && !departamentoArea.getDescricao().isEmpty()
                && departamentoArea.getSigla() != null
                && !departamentoArea.getSigla().isEmpty();
    }

    public Page<DepartamentoArea> buscaPaginada(String descricao, Integer page, Integer linesPerPage, String orderBy, String direction) {
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return departamentoRepositorio.search(descricao, pageRequest);
    }

    public MensagemRetorno excluir(Long id) {
        try {
            this.departamentoRepositorio.deleteById(id);
            return new MensagemRetorno(true, "Sucesso ao excluir");
        } catch (DataIntegrityViolationException ex) {
            return new MensagemRetorno(false, "Não é possível excluir pois este registro está sendo usado");
        }
    }

    public MensagemRetorno editar(DepartamentoArea departamentoArea) {

        if (!this.isDepartamentoValido(departamentoArea))
            return new MensagemRetorno(false, "Dados do Departamento Area inválidos");

        try {
            this.departamentoRepositorio.save(departamentoArea);
            return new MensagemRetorno(true, "Registro atualizado com sucesso!");
        } catch (Exception ex) {
            ex.printStackTrace();
            return new MensagemRetorno(false, ex.getMessage());
        }
    }

    public List<DepartamentoArea> buscarTodos() {
        return this.departamentoRepositorio.findAll();
    }
}
