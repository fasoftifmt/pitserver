package br.edu.ifmt.pit.recursos;


import br.edu.ifmt.pit.dominio.Campus;
import br.edu.ifmt.pit.recursos.utils.URL;
import br.edu.ifmt.pit.servicos.CampusServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/campus")
public class CampusRecurso {

    @Autowired
    private CampusServico campusServico;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Campus> buscar(@PathVariable Integer id) {
        var campus = campusServico.buscar(id);
        return ResponseEntity.ok().body(campus);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping(value = "/inserir")
    public ResponseEntity<MensagemRetorno> inserir(@RequestBody Campus campus) {

        var mensagemRetorno = this.campusServico.salvar(campus);


        if(mensagemRetorno.isSucesso()) {
            return ResponseEntity.ok().body(mensagemRetorno);
        } else {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mensagemRetorno);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value="/pagina")
    public ResponseEntity<Page<Campus>> findPage(
            @RequestParam(value = "nome", defaultValue= "") String nome,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "orderBy", defaultValue = "nome") String orderBy,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction){

        String descricaoDeconded = URL.decodeParam(nome);

        Page<Campus> lista = campusServico.buscaPaginada(descricaoDeconded, page, linesPerPage, orderBy, direction);
        return ResponseEntity.ok().body(lista);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public  ResponseEntity<MensagemRetorno> excluir(@PathVariable Integer id) {

        var msgRetorno = this.campusServico.excluir(id);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping
    public ResponseEntity<MensagemRetorno> editar(@RequestBody Campus campus) {

        var msgRetorno = this.campusServico.salvar(campus);

        return ResponseEntity.ok().body(msgRetorno);
    }

    @GetMapping(value = "/listarTodos")
    public ResponseEntity<List<Campus>> listarTodos(){
        List<Campus> campus = campusServico.listarTodos();
        return ResponseEntity.ok().body(campus);
    }


}
