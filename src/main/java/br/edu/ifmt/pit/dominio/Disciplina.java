package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Disciplina implements Serializable {

    private static final long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String descricao;
    private Double cargaHoraria;
    private int numAulaSemanal;
    private int periodo;


    @ManyToOne
    @JsonIgnoreProperties("disciplinas")
    @JoinColumn(name = "curso_id")
    private Curso curso;

    public Disciplina() {
    }

    public Disciplina(Long id, String descricao, Double cargaHoraria, int numAulaSemanal, int periodo,  Curso curso) {
        this.descricao = descricao;
        this.cargaHoraria = cargaHoraria;
        this.curso = curso;
        this.id = id;
        this.numAulaSemanal = numAulaSemanal;
        this.periodo = periodo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(Double cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public int getNumAulaSemanal() {return numAulaSemanal; }

    public void setNumAulaSemanal(int numAulaSemanal) {this.numAulaSemanal = numAulaSemanal; }

    public int getPeriodo() {return periodo; }

    public void setPeriodo(int periodo) {this.periodo = periodo; }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disciplina that = (Disciplina) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
