package br.edu.ifmt.pit.dominio.enums;

public enum RegimeTrabalho {
    
    VINTE_HORAS(0, "Vinte horas"),
    QUARENTA_HORAS(1, "Quarenta horas"),
    DEDICACAO_EXCLUSIVA(2, "Dedicacao exclusiva");

    private int cod;
    private String descricao;

    RegimeTrabalho(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public int getCod() {
        return cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public static RegimeTrabalho toEnum(Integer cod){
        if (cod == null) {
            return null;
        }

        for (RegimeTrabalho x: RegimeTrabalho.values()) {
            if (cod.equals(x.getCod())) {
                return x;
            }
        }
        throw new IllegalArgumentException("Id inválido: " + cod);
    }

    @Override
    public String toString() {
        return descricao ;
    }
}
