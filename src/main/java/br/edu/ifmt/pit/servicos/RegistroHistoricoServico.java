package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.PlanoIndividualDeTrabalho;
import br.edu.ifmt.pit.dominio.RegistroHistorico;
import br.edu.ifmt.pit.dominio.Servidor;
import br.edu.ifmt.pit.dominio.dtos.RegistroHistoricoDTO;
import br.edu.ifmt.pit.dominio.enums.StatusPit;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.PlanoDeTrabalhoRepositorio;
import br.edu.ifmt.pit.repositorio.interfaces.RegistroHistoricoRepositorio;
import br.edu.ifmt.pit.repositorio.interfaces.ServidorRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RegistroHistoricoServico {

    @Autowired
    private ServidorRepositorio servidorRepositorio;

    @Autowired
    private RegistroHistoricoRepositorio registroRepositorio;

    @Autowired
    private PlanoDeTrabalhoRepositorio planoDeTrabalhoRepositorio;

    public List<RegistroHistorico> todos() {
        return this.registroRepositorio.findAll();
    }

    public RegistroHistorico buscar(Long id) {
        Optional<RegistroHistorico> obj = registroRepositorio.findById(id);
        return obj.orElse(null);
    }


    public MensagemRetorno inserir(RegistroHistoricoDTO registroHistoricodto) {

        RegistroHistorico registroHistorico = this.fromDTO(registroHistoricodto);


        if (!this.isRegistroValido(registroHistorico)) {
            return new MensagemRetorno(false, "Dados do registro inválidos");
        }

        try {
            this.registroRepositorio.save(registroHistorico);
            return new MensagemRetorno(true, "Sucesso ao inserir o registro");
        } catch (Exception e) {
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }
    }


    public boolean isRegistroValido(RegistroHistorico registro) {

        if (registro == null) return false;

        return registro.getDescricao() != null && !registro.getDescricao().isEmpty();
    }


    public RegistroHistorico fromDTO(RegistroHistoricoDTO dto) {

        RegistroHistorico registroHistorico = new RegistroHistorico();
        Optional<PlanoIndividualDeTrabalho> plano = this.planoDeTrabalhoRepositorio.findById(dto.getPitid());
        plano.ifPresent(registroHistorico::setPit);
        registroHistorico.setDescricao(dto.getDescricao());
        registroHistorico.setDataRegistro(dto.getDataRegistro());
        registroHistorico.setStatusPitAnterior(StatusPit.toEnum(dto.getStatusPitAnterior()));
        registroHistorico.setStatusPitAtual(StatusPit.toEnum(dto.getStatusPitAtual()));
        Optional<Servidor> autorAcao = this.servidorRepositorio.findById(dto.getAutorAcaoID());
        autorAcao.ifPresent(registroHistorico::setAutorAcao);
        registroHistorico.setId(dto.getId());

        return registroHistorico;
        /*
[{

    "dataRegistro": "2014-09-27T18:30:49-0400",
    "descricao": "Novo",
    "statusPitAnterior": "Planejado",
    "statusPitAtual": "Planejado",
    "pitid": 1,
    "autorAcaoID": 2

},

{

    "dataRegistro": "2014-09-27T18:30:49-0400",
    "descricao": "Novo",
    "statusPitAnterior": "Planejado",
    "statusPitAtual": "Enviado",
    "pitid": 1,
    "autorAcaoID": 2

},
{

    "dataRegistro": "2014-09-27T18:30:49-0400",
    "descricao": "Novo",
    "statusPitAnterior": "Enviado",
    "statusPitAtual": "Aprovado",
    "pitid": 1,
    "autorAcaoID": 2

},
{

    "dataRegistro": "2014-09-27T18:30:49-0400",
    "descricao": "Novo",
    "statusPitAnterior": "Aprovado",
    "statusPitAtual": "Publicado",
    "pitid": 1,
    "autorAcaoID": 2

},
{

    "dataRegistro": "2014-09-27T18:30:49-0400",
    "descricao": "Novo",
    "statusPitAnterior": "Publicado",
    "statusPitAtual": "Arquivado",
    "pitid": 1,
    "autorAcaoID": 2

}]



         */


    }

}
