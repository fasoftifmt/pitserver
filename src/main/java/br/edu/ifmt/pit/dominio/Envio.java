package br.edu.ifmt.pit.dominio;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Envio implements Serializable {

    private static final Long serialVersionID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEnvio;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-4")
    private Date dataEnvio;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone="GMT-4")
    private Date dataRetorno;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "planoDeTrabalho_id")
    private PlanoIndividualDeTrabalho planoDeTrabalho;

    @OneToMany()
    @JoinColumn(name = "envio_id")
    private List<Avaliacao> avaliacaoList = new ArrayList<>();

    public Envio() {}

    public Envio(Long idEnvio, Date dataEnvio, Date dataRetorno, PlanoIndividualDeTrabalho planoDeTrab) {
        this.dataEnvio = dataEnvio;
        this.dataRetorno = dataRetorno;
        this.idEnvio = idEnvio;
        this.planoDeTrabalho = planoDeTrab;
    }

    public Long getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Long idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public Date getDataRetorno() {
        return dataRetorno;
    }

    public void setDataRetorno(Date dataRetorno) {
        this.dataRetorno = dataRetorno;
    }

    public PlanoIndividualDeTrabalho getPlanoDeTrabalho() {
        return planoDeTrabalho;
    }

    public void setPlanoDeTrabalho(PlanoIndividualDeTrabalho planoDeTrabalhoDocente) {
        this.planoDeTrabalho = planoDeTrabalhoDocente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Envio envio = (Envio) o;
        return Objects.equals(idEnvio, envio.idEnvio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEnvio);
    }
}
