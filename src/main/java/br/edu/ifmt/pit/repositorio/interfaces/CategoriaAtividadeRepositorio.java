package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.CategoriaAtividade;
import br.edu.ifmt.pit.dominio.DepartamentoArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CategoriaAtividadeRepositorio extends JpaRepository<CategoriaAtividade, Integer> {

    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM CategoriaAtividade obj WHERE obj.descricao LIKE %:descricao%")
    Page<CategoriaAtividade> search(@Param("descricao") String descricao, Pageable pageRequest);

}
