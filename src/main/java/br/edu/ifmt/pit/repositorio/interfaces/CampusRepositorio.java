package br.edu.ifmt.pit.repositorio.interfaces;

import br.edu.ifmt.pit.dominio.Campus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CampusRepositorio extends JpaRepository<Campus, Integer> {
    @Transactional(readOnly = true)
    @Query("SELECT DISTINCT obj FROM  Campus obj WHERE obj.nome LIKE %:nome%")
    Page<Campus> search(@Param("nome")String nome, Pageable pageRequest);
}
