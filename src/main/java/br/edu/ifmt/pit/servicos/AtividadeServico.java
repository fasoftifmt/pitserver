package br.edu.ifmt.pit.servicos;

import br.edu.ifmt.pit.dominio.Atividade;
import br.edu.ifmt.pit.dominio.CategoriaAtividade;
import br.edu.ifmt.pit.dominio.dtos.AtividadeDTO;
import br.edu.ifmt.pit.dominio.enums.UnidadeAtividade;
import br.edu.ifmt.pit.recursos.MensagemRetorno;
import br.edu.ifmt.pit.repositorio.interfaces.AtividadeRepositorio;
import br.edu.ifmt.pit.repositorio.interfaces.CategoriaAtividadeRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AtividadeServico {

    @Autowired
    private AtividadeRepositorio atividadeRepositorio;

    @Autowired
    private CategoriaAtividadeRepositorio categoriaAtividadeRepositorio;

    public Atividade buscar(Long id) {
        Optional<Atividade> obj = atividadeRepositorio.findById(id);
        return obj.orElse(null);
    }

    public void salvar(List<Atividade> atividadeList) {
        this.atividadeRepositorio.saveAll(atividadeList);
    }


    public MensagemRetorno inserir(AtividadeDTO atividadeDTO) {

        Atividade atividade = this.fromDTO(atividadeDTO);
        if (!this.isAtividadeValida(atividade)) {
            return new MensagemRetorno(false, "Dados de curso inválidos");
        }

        try {
            this.atividadeRepositorio.save(atividade);
            return new MensagemRetorno(true, "Sucesso ao inserir registro");
        } catch (Exception e) {
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }
    }

    public MensagemRetorno editar(AtividadeDTO atividadeDTO) {
        Atividade atividade = this.fromDTO(atividadeDTO);
        if (!this.isAtividadeValida(atividade)) {
            return new MensagemRetorno(false, "Dados de atividade inválidos");
        }

        try {
            this.atividadeRepositorio.save(atividade);
            return new MensagemRetorno(true, "Registro atualizado com sucesso");

        } catch (Exception e) {
            e.printStackTrace();
            return new MensagemRetorno(false, e.getMessage());
        }

    }


    public boolean isAtividadeValida(Atividade atividade) {
        if (atividade == null) return false;

        return atividade.getDescricao() != null && !atividade.getDescricao().isEmpty();
    }

    public Page<Atividade> buscaPaginada(String descricao, Integer page, Integer linesPerPage,
                                         String orderBy, String direction) {
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return atividadeRepositorio.search(descricao, pageRequest);
    }

    public MensagemRetorno excluir(Long id) {

        try {
            this.atividadeRepositorio.deleteById(id);
            return new MensagemRetorno(true, "Sucesso ao excluir");
        } catch (DataIntegrityViolationException ex) {
            return new MensagemRetorno(false, "Não é possível excluir este registro pois ele está" +
                    "sendo usado");
        }
    }


    public MensagemRetorno vincularAtividades(List<Integer> ids) {
        try {
            if (!ids.isEmpty()) {

                Long atividadeID = Long.valueOf(ids.get(0));
                Long subAtividadeID = Long.valueOf(ids.get(1));

                Atividade atividade = this.buscar(atividadeID);
                Atividade subAtividade = this.buscar(subAtividadeID);
                List<Atividade> listaSubAtividades = atividade.getSubAtividadeList();

                if (!listaSubAtividades.contains(subAtividade)) {
                    listaSubAtividades.add(subAtividade);
                    atividade.setSubAtividadeList(listaSubAtividades);

                    this.atividadeRepositorio.save(atividade);
                }

                return new MensagemRetorno(true, "Sucesso ao realizar vínculo");
            } else {

                return new MensagemRetorno(false, "Erro ao realizar vinculo, pois ja foi realizado anteriormente.");

            }


        } catch (Exception e) {
            return new MensagemRetorno(false, "Erro ao realizar vinculo");
        }
    }

    public MensagemRetorno desvincularAtividades(List<Integer> ids) {
        try {
            if (!ids.isEmpty()) {

                Long atividadeID = Long.valueOf(ids.get(0));
                Long subAtividadeID = Long.valueOf(ids.get(1));

                Atividade atividade = this.buscar(atividadeID);
                Atividade subAtividade = this.buscar(subAtividadeID);
                List<Atividade> listaSubAtividades = atividade.getSubAtividadeList();

                if (listaSubAtividades.contains(subAtividade)) {
                    listaSubAtividades.remove(subAtividade);
                    atividade.setSubAtividadeList(listaSubAtividades);

                    this.atividadeRepositorio.save(atividade);

                } else {
                    return new MensagemRetorno(false, "Erro ao realizar desvinculo, pois ele não existe.");
                }

                return new MensagemRetorno(true, "Sucesso ao realizar desvínculo");
            } else {

                return new MensagemRetorno(false, "Erro ao realizar vinculo, pois ja foi realizado anteriormente.");

            }


        } catch (Exception e) {
            return new MensagemRetorno(false, "Erro ao realizar desvinculo");
        }
    }


    public Atividade fromDTO(AtividadeDTO dto) {

        Atividade atividade = new Atividade();
        atividade.setDescricao(dto.getDescricao());
        atividade.setDicaPreenchimento(dto.getDicaPreenchimento());
        atividade.setFator(dto.getFator());
        atividade.setQtdMax(dto.getQtdMax());
        atividade.setUnidadeAtividade(UnidadeAtividade.valueOf(dto.getUnidadeAtividade()));

        boolean lancamentosMultiplos;

        if (dto.getPermiteLancamentoMultiplos() == 0) {
            lancamentosMultiplos = false;
        } else {
            lancamentosMultiplos = true;
        }
        atividade.setPermiteLancamentoMultiplos(lancamentosMultiplos);

        boolean isSubAtividade;

        if (dto.getSubAtividade() == 0) {
            isSubAtividade = false;
        } else {
            isSubAtividade = true;
        }
        atividade.setSubAtividade(isSubAtividade);


        Optional<CategoriaAtividade> categoriaAtividade = this.categoriaAtividadeRepositorio.findById(dto.getCategoriaID());
        categoriaAtividade.ifPresent(atividade::setCategoria);

        atividade.setId(dto.getId());

        return atividade;
    }


    public List<Atividade> buscarTodos() {
        return this.atividadeRepositorio.findAll();
    }
}
