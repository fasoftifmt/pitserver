package br.edu.ifmt.pit.dominio.dtos;


import java.io.Serializable;

public class AtividadeDTO implements Serializable {

    private static final long serialVersionID = 1L;

    private Long id;

    private Double fator;
    private String descricao;
    private String dicaPreenchimento;
    private String unidadeAtividade;

    private int categoriaID;
    private int subAtividade;
    private Double qtdMax;
    private int permiteLancamentoMultiplos;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFator() {
        return fator;
    }

    public void setFator(Double fator) {
        this.fator = fator;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDicaPreenchimento() {
        return dicaPreenchimento;
    }

    public void setDicaPreenchimento(String dicaPreenchimento) {
        this.dicaPreenchimento = dicaPreenchimento;
    }

    public String getUnidadeAtividade() {
        return unidadeAtividade;
    }

    public void setUnidadeAtividade(String unidadeAtividade) {
        this.unidadeAtividade = unidadeAtividade;
    }

    public int getCategoriaID() {
        return categoriaID;
    }

    public void setCategoria(int categoria) {
        this.categoriaID = categoria;
    }

    public int getSubAtividade() {
        return subAtividade;
    }

    public void setSubAtividade(int subAtividade) {
        this.subAtividade = subAtividade;
    }

    public Double getQtdMax() {
        return qtdMax;
    }

    public void setQtdMax(Double qtdMax) {
        this.qtdMax = qtdMax;
    }

    public int getPermiteLancamentoMultiplos() {
        return permiteLancamentoMultiplos;
    }

    public void setPermiteLancamentoMultiplos(int permiteLancamentoMultiplos) {
        this.permiteLancamentoMultiplos = permiteLancamentoMultiplos;
    }
 }
